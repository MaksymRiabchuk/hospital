<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 8,
    'bsVersion' => '4.x',
    'emptyColor' =>'#99EEA1',
    'lockColor' => '#EEAB99',
    'doctorRecordDeny' => '#FF502B',
];
