<?php
return [
    'language'=>'uk-UA',
    'timeZone' => 'Europe/Kiev',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'patient' => [
            'class' => 'modules\patient\Module'
        ],
        'doctorprofile' => [
            'class' => 'modules\doctorprofile\Module'
        ],
        'laboratory' => [
            'class' => 'modules\laboratory\Module'
        ]

    ],

    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'EUR',
        ],
    ],
];
