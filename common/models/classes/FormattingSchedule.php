<?php

namespace common\models\classes;

use DateInterval;
use DateTime;
use Yii;
use yii\helpers\Url;

class FormattingSchedule
{
    private $startDate; // початкова дата масиву
    private $endDate; // кінцева дата масиву
    private $array = [];
    private $currentDay;
    const INTERVAL=86400;

    public function __construct()
    {
        //Визначити початкову дату масиву
        $this->startDate = $this->getStartDate();
        //Визначити кінцеву дату масиву
        $this->endDate = $this->getEndDate();
        //Заповнити масив
        $this->fillArray();
    }

    private function getStartDate() {
        $firstDayOfMonth=new DateTime('first day of');
        $firstDayOfMonth->modify('-'.($firstDayOfMonth->format('N')-1).' day');
        return $firstDayOfMonth->format('Y-m-d');
    }

    private function getEndDate() {
        $lastDayOfMonth=new DateTime('last day of');
        $lastDayOfMonth->modify('+'.(7-$lastDayOfMonth->format('N')).' day');
        return $lastDayOfMonth->format('Y-m-d');
    }

    private function fillArray() {
        $this->currentDay=date('d.m.Y');
        for ($i=strtotime($this->startDate);$i<=strtotime($this->endDate);$i+=self::INTERVAL){
            if ($i>=strtotime($this->currentDay)){
                $this->array[$i]=['color'=> Yii::$app->params['emptyColor'],'dayTimeStamp'=>$i,'url'=>'/pre-records-doctors/index'];
            }
            else{
                $this->array[$i]=['color'=> Yii::$app->params['lockColor'],'dayTimeStamp'=>$i,'url'=>false];
            }
        }
    }

    public function viewSchedule(): array
    {
        return $this->array;
    }

}