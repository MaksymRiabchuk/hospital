<?php

namespace common\models\classes;

use common\models\entity\PreRecordsDoctors;
use DateTime;
use Exception;
use Yii;
use yii\base\Model;

class FormattingRecord extends Model
{
    public $doctor;
    public $description;
    const MINUTES_INTERVAL=900;
    const EMPTY_COLOR='#99EEA1';
    const LOCK_COLOR='#EEAB99';
    private $array=[];
    private $startTime;
    private $endTime;

    /**
     * @throws Exception
     */
    public function __construct($day, $doctor_id, $config=null)
    {
        parent::__construct($config);
        $this->createTimeDoctorSchedule($doctor_id,$day);
    }

    /**
     * @throws Exception
     */
    public function createTimeDoctorSchedule($doctor_id, $day){
        $this->findStartTime($day);
        $this->findEndTime($day);
        $records=PreRecordsDoctors::find()->where(['doctor_id'=>$doctor_id])->
        andWhere(['between','visit_date',$this->startTime,$this->endTime])->all();
        for($i=$this->startTime;$i<$this->endTime;$i+=self::MINUTES_INTERVAL){
            if (time()>$i){
                $this->array[$i]=['color'=> Yii::$app->params['lockColor']];
            }
            else{
                $this->array[$i]=['color'=> Yii::$app->params['emptyColor']];
            }
        }

        foreach ($records as $record){
            /* @var PreRecordsDoctors $record */
            $this->array[$record->visit_date]=['color'=> Yii::$app->params['lockColor']];
        }
    }

    /**
     * @throws Exception
     */
    private function findStartTime($start_day){
        $selectedDay=new DateTime(date('Y-m-d',$start_day));
        $selectedDay=$selectedDay->format('Y-m-d 00:00');
        $this->startTime=strtotime($selectedDay);
    }

    /**
     * @throws Exception
     */
    private function findEndTime($start_day){
        $selectedDay=new DateTime(date('Y-m-d',$start_day));
        $selectedDay=$selectedDay->format('Y-m-d 23:59');
        $this->endTime=strtotime($selectedDay);
    }
    public function viewTimeSchedule(): array
    {
        return $this->array;
    }

}