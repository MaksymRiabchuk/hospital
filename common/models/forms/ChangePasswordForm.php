<?php /** @noinspection PhpUnused */

namespace common\models\forms;

use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $old_password;
    public $new_password;
    public $confirm_password;

    public function rules(): array
    {
        return [
            [['old_password','new_password','confirm_password'],'required'],
            [['old_password','new_password','confirm_password'],'string','min'=>4],
            [['old_password','new_password','confirm_password'],'required'],
            ['confirm_password', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Паролі не співпадають'],
            ['old_password','checkOldPassword'],
        ];
    }
    public function checkOldPassword(){
        $user= Yii::$app->user->identity;

        if(!Yii::$app->security->validatePassword($this->old_password, $user->password_hash)){
            $this->addError('old_password',Yii::t('app','Не вірний пароль'));
        }

    }
    public function attributeLabels(): array
    {
        return [
            'old_password' => Yii::t('app','Старий пароль'),
            'new_password' => Yii::t('app','Новий пароль'),
            'confirm_password' => Yii::t('app','Підтвердіть пароль'),
        ];
    }
    public function changePassword(){
        $user= Yii::$app->user->identity;
        $user->setPassword($this->new_password);
        $user->save();
    }
}