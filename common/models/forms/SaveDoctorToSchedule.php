<?php /** @noinspection PhpUnused */

namespace common\models\forms;

use yii\base\Model;

class SaveDoctorToSchedule extends Model
{

    public $doctors;
    public $selectedDay;

    public function rules(): array
    {
        return [
            ['doctors','integer'],
            ['selectedDay','string'],
        ];
    }

}