<?php

namespace common\models\search;

use common\models\entity\PreRecordsProcedures;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProcedureSearch represents the model behind the search form of `common\models\entity\PreRecordsProcedures`.
 */
class PreRecordsProceduresSearch extends PreRecordsProcedures
{
    /**
     * {@inheritdoc}
     */
    public function rules():array
    {
        return [
            [['id', 'patient_id', 'doctor_id', 'visit_date', 'procedure_type_id','status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = PreRecordsProcedures::find()->with('doctor')->with('patient')->with('procedureType');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['visit_date' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'patient_id' => $this->patient_id,
            'doctor_id' => $this->doctor_id,
            'visit_date' => $this->visit_date,
            'procedure_type_id' => $this->procedure_type_id,
            'status'=>$this->status,
            'patient_card_history_id' => $this->procedure_type_id,
        ]);

        return $dataProvider;
    }
}
