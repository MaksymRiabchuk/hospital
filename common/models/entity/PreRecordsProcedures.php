<?php /** @noinspection PhpUnusedLocalVariableInspection */
/** @noinspection PhpUnused */

/** @noinspection PhpMissingReturnTypeInspection */

namespace common\models\entity;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * This is the model class for table "pre_records_procedures".
 *
 * @property int $id
 * @property int|null $patient_id Пацієнт
 * @property int|null $doctor_id Лікар
 * @property int|null $visit_date Дата
 * @property int|null $procedure_type_id Назва процедури
 * @property int|null $patient_card_history_id
 * @property int|null $status Статус
 * @property string $description Примітки
 *
 * @property Doctor $doctor
 * @property Patient $patient
 * @property ProcedureTypes $procedureType
 */
class PreRecordsProcedures extends ActiveRecord
{
    /**
     * @var mixed|null
     */
    private $arrayFiles;
    private $attachments;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'pre_records_procedures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['patient_id', 'doctor_id', 'visit_date', 'procedure_type_id', 'status'], 'integer'],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::class, 'targetAttribute' => ['doctor_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::class, 'targetAttribute' => ['patient_id' => 'id']],
            [['procedure_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcedureTypes::class, 'targetAttribute' => ['procedure_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Пацієнт',
            'doctor_id' => 'Лікар',
            'visit_date' => 'Дата',
            'procedure_type_id' => 'Назва процедури',
            'patient_card_history_id' => 'Запис карти пацієнта',
            'status' => 'Статус',
            'description' => 'Примітки',
        ];
    }

    /**
     * Gets query for [[Doctor]].
     *
     * @return ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::class, ['id' => 'doctor_id']);
    }

    /**
     * Gets query for [[Patient]].
     *
     * @return ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::class, ['id' => 'patient_id']);
    }

    /**
     * Gets query for [[ProcedureType]].
     *
     * @return ActiveQuery
     */
    public function getProcedureType()
    {
        return $this->hasOne(ProcedureTypes::class, ['id' => 'procedure_type_id']);
    }
    private function saveProcedureAttachment($procedure_id)
    {
        $array=$_FILES['Procedure']['name'];
        if (isset($array)){
            if(isset($array['attachments'])){
                $firstElement=array_shift($array['attachments']);
                if($firstElement){
                    $this->getProcedureAttachments($procedure_id);
                    return true;
                }
                else{
                    return false;
                }
            }
        }
        return false;

    }
    private function getProcedureAttachments($procedure_id): void
    {
        isset($_FILES['Procedure']['name']) ? $this->arrayFiles = $_FILES['Procedure'] : $this->arrayFiles = null;
        if (!$this->arrayFiles == []) {
            $this->setFileNames();
            $this->setFileSize();
            $this->setTmpName();
            $this->setTypeFile();
            $this->setErrorFiles();
            $path = dirname(__DIR__, 3) . '/backend/web/uploads/';
            foreach ($this->attachments as $key => $item) {
                $model = new ProcedureVisitsAttachments();
                /* @var $model ProcedureVisitsAttachments */
                move_uploaded_file($item['tmp_name'], $path . $item['name']);
                $model->procedure_visit_id = $procedure_id;
                $model->file = $path . $item['name'];
                if (!$model->save()){
                    Yii::info($model->errors);
                }
            }
        }
    }

    /**
     * @throws Exception
     */
    public function saveProcedure(){
        $transaction=Yii::$app->db->beginTransaction();
        $this->visit_date=strtotime($this->visit_date);
        $procedure=$this->save();
        if(!$procedure){
            $transaction->rollBack();
            throw new Exception('Помилка збереження');
        }
        if (!$this->saveProcedureAttachment($this->id)){
            $transaction->rollBack();
            throw new Exception('Помилка збереження');
        }
        $transaction->commit();
        return true;
    }

    private function setFileNames()
    {
        if ($this->arrayFiles) {
            foreach ($this->arrayFiles['name']['attachments'] as $key => $item) {
                $this->attachments[$key]['name'] = time().rand(0,999999999999).$item;
            }
        }
    }

    private function setFileSize()
    {
        if ($this->arrayFiles) {
            foreach ($this->arrayFiles['size']['attachments'] as $key => $item) {
                $this->attachments[$key]['size'] = $item;
            }
        }
    }

    private function setTmpName()
    {
        if ($this->arrayFiles) {
            foreach ($this->arrayFiles['tmp_name']['attachments'] as $key => $item) {
                $this->attachments[$key]['tmp_name'] = $item;
            }
        }
    }

    private function setTypeFile()
    {
        if ($this->arrayFiles) {
            foreach ($this->arrayFiles['type']['attachments'] as $key => $item) {
                $this->attachments[$key]['type'] = $item;
            }
        }
    }

    private function setErrorFiles()
    {
        if ($this->arrayFiles) {
            foreach ($this->arrayFiles['error']['attachments'] as $key => $item) {
                $this->attachments[$key]['error'] = $item;
            }
        }
    }
}
