<?php /** @noinspection PhpUnused */

namespace common\models\entity;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "procedure_types".
 *
 * @property int $id
 * @property string $name Назва
 * @property string|null $description Додатковий опис
 *
 * @property PreRecordsProcedures[] $preRecordsProcedures
 * @property ProcedureVisits[] $procedureVisits
 */
class ProcedureTypes extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'procedure_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'description' => 'Додатковий опис',
        ];
    }

    /**
     * Gets query for [[PreRecordsProcedures]].
     *
     * @return ActiveQuery
     */
    public function getPreRecordsProcedures(): ActiveQuery
    {
        return $this->hasMany(PreRecordsProcedures::class, ['procedure_type_id' => 'id']);
    }

    public static function getProcedureTypesById(): array
    {
        return ArrayHelper::map(self::find()->all(),'id','name');
    }

    /**
     * Gets query for [[ProcedureVisits]].
     *
     * @return ActiveQuery
     */
    public function getProcedureVisits(): ActiveQuery
    {
        return $this->hasMany(ProcedureVisits::class, ['procedure_type_id' => 'id']);
    }
}
