<?php /** @noinspection PhpUnused */

namespace common\models\entity;

use common\models\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "laboratory_assistants".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $parent_name
 * @property string|null $image
 * @property int|null $user_id
 *
 * @property User $user
 */
class LaboratoryAssistant extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'laboratory_assistants';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['user_id'], 'integer'],
            [['name', 'surname', 'parent_name', 'image'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'parent_name' => 'Parent Name',
            'image' => 'Image',
            'user_id' => 'User ID',
        ];
    }


    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
    public function getFullLaboratoryAssistantName(): string
    {
        return $this->surname." ".$this->name." ".$this->parent_name;
    }
    public function getShortLaboratoryAssistantName(): string
    {
        return $this->surname." ".mb_substr($this->name,0,1).".".mb_substr($this->parent_name,0,1).".";
    }
    public function getFullName(): string
    {
        return $this->surname." ".$this->name." ".$this->parent_name;
    }
}
