<?php /** @noinspection PhpUnused */

namespace common\models\entity;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "procedure_visits".
 *
 * @property int $id
 * @property int $patient_id Пацієнт
 * @property int $laboratory_assistant_id Лаборант
 * @property int $procedure_date Дата процедури
 * @property int $procedure_type_id Тип процедури
 * @property string $description Нотатки
 * @property int $status Статус
 *
 * @property LaboratoryAssistant $laboratoryAssistant
 * @property Patient $patient
 * @property ProcedureTypes $procedureType
 * @property ProcedureVisitsAttachments[] $procedureVisitsAttachments
 */
class ProcedureVisits extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'procedure_visits';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['patient_id', 'laboratory_assistant_id', 'procedure_date', 'procedure_type_id', 'description'], 'required'],
            [['patient_id', 'laboratory_assistant_id', 'procedure_date', 'procedure_type_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['laboratory_assistant_id'], 'exist', 'skipOnError' => true, 'targetClass' => LaboratoryAssistant::class, 'targetAttribute' => ['laboratory_assistant_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::class, 'targetAttribute' => ['patient_id' => 'id']],
            [['procedure_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcedureTypes::class, 'targetAttribute' => ['procedure_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Пацієнт',
            'laboratory_assistant_id' => 'Лаборант',
            'procedure_date' => 'Дата процедури',
            'procedure_type_id' => 'Тип процедури',
            'description' => 'Нотатки',
            'status' => 'Статус',
        ];
    }

    /**
     * Gets query for [[LaboratoryAssistant]].
     *
     * @return ActiveQuery
     */
    public function getLaboratoryAssistant(): ActiveQuery
    {
        return $this->hasOne(LaboratoryAssistant::class, ['id' => 'laboratory_assistant_id']);
    }

    /**
     * Gets query for [[Patient]].
     *
     * @return ActiveQuery
     */
    public function getPatient(): ActiveQuery
    {
        return $this->hasOne(Patient::class, ['id' => 'patient_id']);
    }

    /**
     * Gets query for [[ProcedureType]].
     *
     * @return ActiveQuery
     */
    public function getProcedureType(): ActiveQuery
    {
        return $this->hasOne(ProcedureTypes::class, ['id' => 'procedure_type_id']);
    }

    /**
     * Gets query for [[ProcedureVisitsAttachments]].
     *
     * @return ActiveQuery
     */
    public function getProcedureVisitsAttachments(): ActiveQuery
    {
        return $this->hasMany(ProcedureVisitsAttachments::class, ['procedure_visit_id' => 'id']);
    }
}
