<?php /** @noinspection PhpUnused */

namespace common\models\entity;

use common\models\enums\RoleTypes;
use common\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "doctors".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $parent_name
 * @property int|null $specialization_id
 * @property string|null $image
 * @property int|null $user_id
 *
 * @property Specialization $specialization
 * @property User $user
 */
class Doctor extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'doctors';
    }
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['specialization_id', 'user_id'], 'integer'],
            [['name', 'surname', 'parent_name', 'image'], 'string', 'max' => 255],
            [['specialization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Specialization::class, 'targetAttribute' => ['specialization_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app','Ім`я'),
            'surname' => Yii::t('app','Прізвище'),
            'parent_name' => Yii::t('app','По батькові'),
            'specialization_id' => Yii::t('app','Спеціалізація'),
            'image' => Yii::t('app','Фото'),
            'user_id' => Yii::t('app','ID користувача'),
        ];
    }

    /**
     * Gets query for [[Specialization]].
     *
     * @return ActiveQuery
     */
    public function getSpecialization(): ActiveQuery
    {
        return $this->hasOne(Specialization::class, ['id' => 'specialization_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
    public function downloadPhoto(): bool
    {
        if(!$this->image){
            $this->image = '';
        }
        if ($this->save()) {
            $image = UploadedFile::getInstance($this, 'imageFile');
            if ($image) {
                $image->saveAs('uploads/doctors' . $this->id . '.' . $image->getExtension());
                $this->image = '/uploads/doctors' . $this->id . '.' . $image->getExtension();
                $this->save();
            }
        }
        return true;
    }
    public static function getDoctorsList(): array
    {
        return ArrayHelper::map(self::find()->all(),'id','fullDoctorName');
    }
    public function getFullDoctorName(): string
    {
        return $this->surname." ".$this->name." ".$this->parent_name;
    }
    public function getShortDoctorName(): string
    {
        return $this->surname." ".mb_substr($this->name,0,1).".".mb_substr($this->parent_name,0,1).".";
    }
    public static function getAllDoctorId(): array
    {
        return User::find()->where(['role'=>RoleTypes::DOCTOR])->all();
    }
    public static function getDoctorNameById($id): string
    {
        $doctor=Doctor::find()->where(['id'=>$id])->one();
        /* @var $doctor Doctor */
        if($doctor){
            return $doctor->surname.' '.$doctor->name.' '.$doctor->parent_name;
        }
        else{
            return Yii::t('app','Такого лікаря немає');
        }
    }
    public function getFullName(): string
    {
        return $this->surname." ".$this->name." ".$this->parent_name;
    }
}
