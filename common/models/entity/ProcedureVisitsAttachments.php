<?php /** @noinspection PhpUnused */

namespace common\models\entity;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "procedure_visits_attachments".
 *
 * @property int $id
 * @property int $procedure_visit_id
 * @property string $file
 *
 * @property ProcedureVisits $procedureVisit
 */
class ProcedureVisitsAttachments extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'procedure_visits_attachments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['procedure_visit_id', 'file'], 'required'],
            [['procedure_visit_id'], 'integer'],
            [['file'], 'string', 'max' => 255],
            [['procedure_visit_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcedureVisits::class, 'targetAttribute' => ['procedure_visit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'procedure_visit_id' => 'Procedure Visit ID',
            'file' => 'File',
        ];
    }

    /**
     * Gets query for [[ProcedureVisit]].
     *
     * @return ActiveQuery
     */
    public function getProcedureVisit(): ActiveQuery
    {
        return $this->hasOne(ProcedureVisits::class, ['id' => 'procedure_visit_id']);
    }
}
