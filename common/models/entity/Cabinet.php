<?php

namespace common\models\entity;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cabinets".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int|null $is_deleted
 */
class Cabinet extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'cabinets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['is_deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' => Yii::t('app','Назва'),
            'description' => Yii::t('app','Опис'),
            'is_deleted' => Yii::t('app','Видалено'),
        ];
    }
}
