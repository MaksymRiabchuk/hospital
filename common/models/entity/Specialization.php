<?php /** @noinspection PhpUnused */

namespace common\models\entity;

use common\models\query\SpecializationQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "specializations".
 *
 * @property int $id
 * @property string $name
 * @property int $department_id
 * @property int|null $is_deleted
 *
 * @property Department $department
 * @property Doctor[] $doctors
 */
class Specialization extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'specializations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'department_id'], 'required'],
            [['department_id', 'is_deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::class, 'targetAttribute' => ['department_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app','Назва') ,
            'department_id' => Yii::t('app','Відділення'),
            'is_deleted' => Yii::t('app','Видалено'),
        ];
    }

    /**
     * Gets query for [[Department]].
     *
     * @return ActiveQuery
     */
    public function getDepartment(): ActiveQuery
    {
        return $this->hasOne(Department::class, ['id' => 'department_id']);
    }

    /**
     * Gets query for [[Doctors]].
     *
     */
    public function getDoctors(): ActiveQuery
    {
        return $this->hasMany(Doctor::class, ['specialization_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SpecializationQuery the active query used by this AR class.
     */
    public static function find(): SpecializationQuery
    {
        return new SpecializationQuery(get_called_class());
    }
    public static function getSpecializationsById(): array
    {
        return ArrayHelper::map(self::find()->all(),'id','name');
    }
    public static function findSpecializationById($specialization_id){
        return Specialization::find()->where(['id'=>$specialization_id])->one();
    }
}
