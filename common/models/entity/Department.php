<?php /** @noinspection PhpUnused */

namespace common\models\entity;

use common\models\query\DepartmentsQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "department".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property int|null $is_deleted
 *
 * @property Specialization[] $specializations
 */
class Department extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'departments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'address'], 'required'],
            [['is_deleted'], 'integer'],
            [['name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app','ID'),
            'name' =>  Yii::t('app','Назва'),
            'address' => Yii::t('app','Адрес'),
            'is_deleted' => Yii::t('app','Видалено'),
        ];
    }

    /**
     * Gets query for [[Specializations]].
     *
     * @return ActiveQuery
     */
    public function getSpecializations(): ActiveQuery
    {
        return $this->hasMany(Specialization::class, ['department_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return DepartmentsQuery the active query used by this AR class.
     */
    public static function find(): DepartmentsQuery
    {
        return new DepartmentsQuery(get_called_class());
    }
    public static function getDepartmentsById(): array
    {
        return ArrayHelper::map(self::find()->all(),'id','name');
    }
}
