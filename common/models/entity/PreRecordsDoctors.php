<?php /** @noinspection PhpUnused */

namespace common\models\entity;

use common\models\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pre_records_doctors".
 *
 * @property int $id
 * @property int|null $doctor_id Лікар
 * @property int|null $user_id Пацієнт
 * @property int $visit_date Дата візиту
 * @property int $status Статус
 * @property Doctor $doctor
 * @property PatientCardHistory $patient_card_history_id
 * @property User $user
 */
class PreRecordsDoctors extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'pre_records_doctors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['doctor_id', 'user_id', 'visit_date', 'status'], 'integer'],
            [['visit_date'], 'required'],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::class, 'targetAttribute' => ['doctor_id' => 'id']],
            [['patient_card_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => PatientCardHistory::class, 'targetAttribute' => ['patient_card_history_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'doctor_id' => 'Лікар',
            'patient_card_history_id' => 'PatientCardHistoryId',
            'user_id' => 'Пацієнт',
            'visit_date' => 'Дата візиту',
            'status' => 'Статус',
        ];
    }

    /**
     * Gets query for [[Doctor]].
     *
     * @return ActiveQuery
     */
    public function getDoctor(): ActiveQuery
    {
        return $this->hasOne(Doctor::class, ['id' => 'doctor_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
    public function getPatientCardHistory(): ActiveQuery
    {
        return $this->hasOne(PatientCardHistory::class, ['id' => 'patient_card_history_id']);
    }
}
