<?php /** @noinspection PhpUnused */

namespace common\models\entity;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "doctor_visits".
 *
 * @property int $id
 * @property int $patient_id Пацієнт
 * @property int $doctor_id Лікар
 * @property int $visit_date Дата візиту
 * @property string $description Нотатки
 *
 * @property Doctor $doctor
 * @property Patient $patient
 */
class DoctorVisits extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'doctor_visits';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['patient_id', 'doctor_id', 'visit_date', 'description'], 'required'],
            [['patient_id', 'doctor_id', 'visit_date'], 'integer'],
            [['description'], 'string'],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::class, 'targetAttribute' => ['doctor_id' => 'id']],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::class, 'targetAttribute' => ['patient_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Пацієнт',
            'doctor_id' => 'Лікар',
            'visit_date' => 'Дата візиту',
            'description' => 'Нотатки',
        ];
    }

    /**
     * Gets query for [[Doctor]].
     *
     * @return ActiveQuery
     */
    public function getDoctor(): ActiveQuery
    {
        return $this->hasOne(Doctor::class, ['id' => 'doctor_id']);
    }

    /**
     * Gets query for [[Patient]].
     *
     * @return ActiveQuery
     */
    public function getPatient(): ActiveQuery
    {
        return $this->hasOne(Patient::class, ['id' => 'patient_id']);
    }
}
