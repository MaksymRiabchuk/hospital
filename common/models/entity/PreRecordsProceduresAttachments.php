<?php

namespace common\models\entity;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "pre_records_procedures_attachments".
 *
 * @property int $id
 * @property int|null $pre_record_procedure_id
 * @property string|null $file Вкладення
 * @property string $name Назва файлу
 *
 * @property PreRecordsProcedures $preRecordProcedure
 */
class PreRecordsProceduresAttachments extends ActiveRecord
{
    public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pre_records_procedures_attachments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pre_record_procedure_id'], 'integer'],
            [['name'], 'required'],
            [['imageFile'],'safe'],
            [['name'], 'string', 'max' => 255],
            [['pre_record_procedure_id'], 'exist', 'skipOnError' => true, 'targetClass' => PreRecordsProcedures::class, 'targetAttribute' => ['pre_record_procedure_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pre_record_procedure_id' => 'Pre Record Procedure ID',
            'file' => 'Вкладення',
            'name' => 'Назва файлу',
        ];
    }

    /**
     * Gets query for [[PreRecordProcedure]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPreRecordProcedure()
    {
        return $this->hasOne(PreRecordsProcedures::class, ['id' => 'pre_record_procedure_id']);
    }

    public function saveAttachment($record_id): bool
    {
        $this->pre_record_procedure_id=$record_id;
        if(!$this->file){
            $this->file = '';
        }
        if ($this->save()) {
            $image = UploadedFile::getInstance($this, 'imageFile');
            if ($image) {
                $image->saveAs('uploads/laboratoryFiles/' . $this->id . '.' . $image->getExtension());
                $this->file = '/uploads/laboratoryFiles/' . $this->id . '.' . $image->getExtension();
                $this->save();
            }
        }
        return true;
    }
}
