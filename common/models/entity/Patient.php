<?php /** @noinspection PhpUnused */

namespace common\models\entity;

use common\models\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "patient".
 *
 * @property int $id
 * @property string $name Імʼя
 * @property string $surname Прізвище
 * @property string $parent_name По батькові
 * @property string|null $image Фото
 * @property int|null $user_id
 *
 * @property User $user
 */
class Patient extends ActiveRecord
{
    public $imageFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'patients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'surname', 'parent_name'], 'required'],
            [['user_id'], 'integer'],
            [['name', 'surname', 'parent_name', 'image'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Імʼя',
            'surname' => 'Прізвище',
            'parent_name' => 'По батькові',
            'image' => 'Фото',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getFullPatientName(): string
    {
        return $this->surname . " " . $this->name . " " . $this->parent_name;
    }
    public function getShortPatientName(): string
    {
        return $this->surname." ".mb_substr($this->name,0,1).".".mb_substr($this->parent_name,0,1).".";
    }
    public static function getPatientsList(): array
    {
        return ArrayHelper::map(self::find()->all(),'id','fullPatientName');
    }
    public static function findPatientByUserId($user_id){
        return self::find()->where(['user_id'=>$user_id])->one();
    }
    public function downloadPhoto(): bool
    {
        if(!$this->image){
            $this->image = '';
        }
        if ($this->save()) {
            $image = UploadedFile::getInstance($this, 'imageFile');
            if ($image) {
                $image->saveAs('uploads/patients' . $this->id . '.' . $image->getExtension());
                $this->image = '/uploads/patients' . $this->id . '.' . $image->getExtension();
                $this->save();
            }
        }
        return true;
    }
}
