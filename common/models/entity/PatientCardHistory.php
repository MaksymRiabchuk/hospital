<?php /** @noinspection PhpUnused */

/** @noinspection PhpMissingReturnTypeInspection */

namespace common\models\entity;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "patient_card_history".
 *
 * @property int $id
 * @property int $visit_date Дата візиту
 * @property int|null $object_type Тип обʼєкта
 * @property int|null $object_id ID обʼєкта
 * @property int $patient_id Пацієнт
 * @property string $description Опис
 *
 * @property Patient $patient
 */
class PatientCardHistory extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'patient_card_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['visit_date', 'patient_id',], 'required'],
            [['visit_date', 'object_type', 'object_id', 'patient_id'], 'integer'],
            [['description'], 'string'],
            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::class, 'targetAttribute' => ['patient_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'visit_date' => 'Дата візиту',
            'object_type' => 'Тип обʼєкта',
            'object_id' => 'ID обʼєкта',
            'patient_id' => 'Пацієнт',
            'description' => 'Опис',
        ];
    }

    /**
     * Gets query for [[Patient]].
     *
     * @return ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::class, ['id' => 'patient_id']);
    }
}
