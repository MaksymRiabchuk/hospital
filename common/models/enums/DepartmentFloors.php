<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class DepartmentFloors extends BaseEnum
{
    const FIRST_FLOOR = '1 поверх';
    const SECOND_FLOOR = '2 поверх';
    const THIRD_FLOOR = '3 поверх';
    const FOURTH_FLOOR = '4 поверх';
    const FIFTH_FLOOR = '5 поверх';
    const SIXTH_FLOOR = '6 поверх';
    const SEVENTH_FLOOR = '7 поверх';
    const EIGHTH_FLOOR = '8 поверх';

    public static $list = [
        self::FIRST_FLOOR => '1 поверх',
        self::SECOND_FLOOR => '2 поверх',
        self::THIRD_FLOOR => '3 поверх',
        self::FOURTH_FLOOR => '4 поверх',
        self::FIFTH_FLOOR => '5 поверх',
        self::SIXTH_FLOOR => '6 поверх',
        self::SEVENTH_FLOOR => '7 поверх',
        self::EIGHTH_FLOOR => '8 поверх',
    ];
}