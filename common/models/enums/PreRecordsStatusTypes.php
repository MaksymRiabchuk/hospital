<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class PreRecordsStatusTypes extends BaseEnum
{
    const IN_WAIT = 0;
    const COME = 1;
    const DIDNT_COME = 2;

    public static $list = [
        self::IN_WAIT => 'В очікувані',
        self::COME => 'Прийшов',
        self::DIDNT_COME => 'Не прийшов',
    ];
}