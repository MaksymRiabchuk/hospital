<?php /** @noinspection ALL */

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class ObjectTypesInPatientCard extends BaseEnum
{
    const DOCTOR = 1;
    const LABORATORY_ASSISTANT = 2;

    public static $list = [
        self::DOCTOR => 'Обʼєкт лікар',
        self::LABORATORY_ASSISTANT => 'Обʼєкт лаборант',
    ];
}