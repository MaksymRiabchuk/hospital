<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class ProcedureVisitStatus extends BaseEnum
{
    const DIRECTED=0;
    const IN_PROCESS = 1;
    const PROCESSED = 2;
    const DECLINE=3;

    public static $list = [
        self::DIRECTED=>'Направлено',
        self::IN_PROCESS => 'Не оброблено',
        self::PROCESSED => 'Оброблено',
        self::DECLINE=> 'Відмовлено',
    ];
}