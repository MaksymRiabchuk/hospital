<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class StatusTypes extends BaseEnum
{
    const DELETED = 0;
    const INACTIVE = 9;
    const ACTIVE = 10;

    public static $list = [
        self::DELETED => 'Видалений',
        self::INACTIVE => 'Неактивний',
        self::ACTIVE => 'Активний',
    ];
}