<?php

namespace common\models\enums;

use yii2mod\enum\helpers\BaseEnum;

class RoleTypes extends BaseEnum
{
    const ADMIN = 0;
    const DOCTOR = 1;
    const PATIENT = 2;
    const LABORATORY_ASSISTANT=3;

    public static $list = [
        self::ADMIN => 'Адмін',
        self::DOCTOR => 'Лікар',
        self::PATIENT => 'Пацієнт',
        self::LABORATORY_ASSISTANT=>'Лаборант'
    ];
}