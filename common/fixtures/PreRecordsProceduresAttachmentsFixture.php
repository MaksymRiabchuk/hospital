<?php /** @noinspection PhpUnused */

namespace common\fixtures;
use yii\test\ActiveFixture;
class PreRecordsProceduresAttachmentsFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\PreRecordsProceduresAttachments';
    public $dataFile = 'common/tests/_data/pre-records-procedures-attachments.php';
    public $depends = ['common\fixtures\PreRecordsProceduresFixture'];
}
