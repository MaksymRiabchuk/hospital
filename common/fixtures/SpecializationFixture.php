<?php /** @noinspection PhpUnused */

namespace common\fixtures;

use yii\test\ActiveFixture;

class SpecializationFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\Specialization';
    public $dataFile = 'common/tests/_data/specialization.php';
}