<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class DoctorFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\Doctor';
    public $dataFile = 'common/tests/_data/doctor.php';
    public $depends = ['common\fixtures\UserFixture'];
}
