<?php /** @noinspection PhpUnused */

namespace common\fixtures;

use yii\test\ActiveFixture;

class LaboratoryAssistantFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\LaboratoryAssistant';
    public $dataFile = 'common/tests/_data/laboratory-assistant.php';
    public $depends = ['common\fixtures\UserFixture'];
}
