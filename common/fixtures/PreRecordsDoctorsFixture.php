<?php /** @noinspection PhpUnused */

namespace common\fixtures;

use yii\test\ActiveFixture;

class PreRecordsDoctorsFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\PreRecordsDoctors';
    public $dataFile = 'common/tests/_data/pre-records-doctors.php';
    public $depends = ['common\fixtures\UserFixture','common\fixtures\DoctorFixture'];
}
