<?php /** @noinspection PhpUnused */

namespace common\fixtures;

use yii\test\ActiveFixture;

class CabinetFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\Cabinet';
    public $dataFile = 'common/tests/_data/cabinet.php';
}
