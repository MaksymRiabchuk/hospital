<?php /** @noinspection PhpUnused */

namespace common\fixtures;

use yii\test\ActiveFixture;

class ProcedureTypesFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\ProcedureTypes';
    public $dataFile = 'common/tests/_data/procedure-types.php';
}
