<?php /** @noinspection PhpUnused */

namespace common\fixtures;

use yii\test\ActiveFixture;

class PatientFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\Patient';
    public $dataFile = 'common/tests/_data/patient.php';
    public $depends = ['common\fixtures\UserFixture'];
}
