<?php /** @noinspection PhpUnused */

namespace common\fixtures;

use yii\test\ActiveFixture;

class PatientCardHistoryFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\PatientCardHistory';
    public $dataFile = 'common/tests/_data/patient-card-history.php';
    public $depends = ['common\fixtures\LaboratoryAssistantFixture','common\fixtures\DoctorFixture','common\fixtures\PatientFixture'];
}
