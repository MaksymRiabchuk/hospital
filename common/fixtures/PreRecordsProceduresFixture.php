<?php /** @noinspection PhpUnused */

namespace common\fixtures;
use yii\test\ActiveFixture;
class PreRecordsProceduresFixture extends ActiveFixture
{
    public $modelClass = 'common\models\entity\PreRecordsProcedures';
    public $dataFile = 'common/tests/_data/pre-records-procedures.php';
    public $depends = ['common\fixtures\UserFixture'];
}
