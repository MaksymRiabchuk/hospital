<?php

use common\models\enums\PreRecordsStatusTypes;

/**
 * @throws Exception
 */
function getRandomCreatedAt(): int
{
    $dateTime = new DateTime(date('Y-m-d H:00'));
    $interval = new DateInterval('P0Y0DT0H' . rand(1, 11) * 15 . 'M');
    $dateTime->add($interval);
    return $dateTime->getTimestamp();
}

return [
    [
        'id' => 1,
        'doctor_id' => 1,
        'user_id' => 12,
        'visit_date' => getRandomCreatedAt(),
        'status' =>PreRecordsStatusTypes::IN_WAIT
    ],
    [
        'id' => 2,
        'doctor_id' => 1,
        'user_id' => 13,
        'visit_date' => getRandomCreatedAt(),
        'status' =>PreRecordsStatusTypes::IN_WAIT
    ],
    [
        'id' => 3,
        'doctor_id' => 1,
        'user_id' => 14,
        'visit_date' => getRandomCreatedAt(),
        'status' =>PreRecordsStatusTypes::IN_WAIT
    ],
    [
        'id' => 4,
        'doctor_id' => 2,
        'user_id' => 12,
        'visit_date' => getRandomCreatedAt(),
        'status' =>PreRecordsStatusTypes::IN_WAIT
    ],
    [
        'id' => 5,
        'doctor_id' => 2,
        'user_id' => 13,
        'visit_date' => getRandomCreatedAt(),
        'status' =>PreRecordsStatusTypes::IN_WAIT
    ],
    [
        'id' => 6,
        'doctor_id' => 2,
        'user_id' => 14,
        'visit_date' => getRandomCreatedAt(),
        'status' =>PreRecordsStatusTypes::IN_WAIT
    ],
    [
        'id' => 7,
        'doctor_id' => 3,
        'user_id' => 12,
        'visit_date' => getRandomCreatedAt(),
        'status' =>PreRecordsStatusTypes::IN_WAIT
    ],
    [
        'id' => 8,
        'doctor_id' => 3,
        'user_id' => 13,
        'visit_date' => getRandomCreatedAt(),
        'status' =>PreRecordsStatusTypes::IN_WAIT
    ],
    [
        'id' => 9,
        'doctor_id' => 3,
        'user_id' => 14,
        'visit_date' => getRandomCreatedAt(),
        'status' =>PreRecordsStatusTypes::IN_WAIT
    ],
];
