<?php

use common\models\enums\ObjectTypesInPatientCard;
use common\models\enums\PreRecordsStatusTypes;

/**
 * @throws Exception
 */
$faker=Faker\Factory::create();
function getRandomVisitDatePatientCardHistory(): int
{
    $dateTime = new DateTime(date('Y-m-d H:00'));
    $interval = new DateInterval('P0Y0DT0H' . rand(1, 11) * 15 . 'M');
    $dateTime->add($interval);
    return $dateTime->getTimestamp();
}

return [
    [
        'id' => 1,
        'visit_date' => getRandomVisitDatePatientCardHistory(),
        'object_type' => ObjectTypesInPatientCard::DOCTOR,
        'object_id'=>1,
        'patient_id' =>1,
        'description'=>'',
    ],
    [
        'id' => 2,
        'visit_date' => getRandomVisitDatePatientCardHistory(),
        'object_type' => ObjectTypesInPatientCard::DOCTOR,
        'object_id'=>1,
        'patient_id' =>2,
        'description'=>$faker->text(20),
    ],
    [
        'id' => 3,
        'visit_date' => getRandomVisitDatePatientCardHistory(),
        'object_type' => ObjectTypesInPatientCard::LABORATORY_ASSISTANT,
        'object_id'=>1,
        'patient_id' =>2,
        'description'=>'',
    ],
    [
        'id' => 4,
        'visit_date' => getRandomVisitDatePatientCardHistory(),
        'object_type' => ObjectTypesInPatientCard::LABORATORY_ASSISTANT,
        'object_id'=>1,
        'patient_id' =>2,
        'description'=>$faker->text(20),
    ],
    [
        'id' => 5,
        'visit_date' => getRandomVisitDatePatientCardHistory(),
        'object_type' => ObjectTypesInPatientCard::DOCTOR,
        'object_id'=>2,
        'patient_id' =>2,
        'description'=>'',
    ],
    [
        'id' => 6,
        'visit_date' => getRandomVisitDatePatientCardHistory(),
        'object_type' => ObjectTypesInPatientCard::DOCTOR,
        'object_id'=>2,
        'patient_id' =>2,
        'description'=>$faker->text(20),
    ],
    [
        'id' => 7,
        'visit_date' => getRandomVisitDatePatientCardHistory(),
        'object_type' => ObjectTypesInPatientCard::LABORATORY_ASSISTANT,
        'object_id'=>2,
        'patient_id' =>2,
        'description'=>'',
    ],
    [
        'id' => 8,
        'visit_date' => getRandomVisitDatePatientCardHistory(),
        'object_type' => ObjectTypesInPatientCard::LABORATORY_ASSISTANT,
        'object_id'=>2,
        'patient_id' =>2,
        'description'=>$faker->text(20),
    ],
];
