<?php
$faker = Faker\Factory::create();

return [
    [
        'id' => 1,
        'name' =>$faker->name,
        'surname' => $faker->lastName,
        'parent_name' => 'Вікторович',
        'image'=>'',
        'user_id'=>22,
    ],
    [
        'id' => 2,
        'name' =>$faker->name,
        'surname' => $faker->lastName,
        'parent_name' => 'Васильович',
        'image'=>'',
        'user_id'=>23,
    ],
];
