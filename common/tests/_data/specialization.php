<?php

return [
    [
        'id' => 1,
        'name' => 'Травматолог',
        'department_id' => 1,
        'is_deleted' => 0,
    ],
    [
        'id' => 2,
        'name' => 'Венеролог',
        'department_id' => 2,
        'is_deleted' => 0,
    ],
    [
        'id' => 3,
        'name' => 'Хірург',
        'department_id' => 3,
        'is_deleted' => 0,
    ],
    [
        'id' => 4,
        'name' => 'Терапевт',
        'department_id' => 4,
        'is_deleted' => 0,
    ],
    [
        'id' => 5,
        'name' => 'Лаборант',
        'department_id' => 5,
        'is_deleted' => 0,
    ],
];
