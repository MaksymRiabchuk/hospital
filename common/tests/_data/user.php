<?php

use common\models\enums\RoleTypes;
$faker = Faker\Factory::create();
return [
    [
        'id' => 1,
        'username' => 'admin',
        //password_admin
        'auth_key' => 'f3ee2RK__BVQrixGIUU6Cw2Gh3txFvRz',
        'password_hash' => '$2y$13$6Y9KxakAzgVPho3M8MpDFu5ngyH7wUutoEkv7dObd26iKOLlac/MG',
        'email' => 'admin@gmail.com',
        'phone' => '123456789',
        'role' => RoleTypes::ADMIN,
        'created_at' => '2022.05.20',
        'updated_at' => '2022.05.20',
    ],
    [
        'id' => 2,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2022.04.29',
        'updated_at' => '2022.04.29',
        'username' => 'doctor',
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 3,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2022.04.20',
        'updated_at' => '2022.04.20',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 4,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2022.01.20',
        'updated_at' => '2022.01.20',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 5,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2022.02.20',
        'updated_at' => '2022.02.20',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 6,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2022.06.20',
        'updated_at' => '2022.06.20',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 7,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2022.03.20',
        'updated_at' => '2022.03.20',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 8,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2022.07.20',
        'updated_at' => '2022.07.20',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 9,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2021.04.20',
        'updated_at' => '2021.04.20',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 10,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2022.05.21',
        'updated_at' => '2022.05.21',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 11,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::DOCTOR,
        'created_at' => '2022.04.22',
        'updated_at' => '2022.04.22',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 12,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2021.04.19',
        'updated_at' => '2021.04.19',
        'username' => 'patient',
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 13,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2022.05.19',
        'updated_at' => '2022.05.19',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 14,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2019.04.22',
        'updated_at' => '2019.04.22',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 15,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2021.04.19',
        'updated_at' => '2021.04.19',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 16,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2022.05.19',
        'updated_at' => '2022.05.19',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 17,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2019.04.22',
        'updated_at' => '2019.04.22',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 18,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2022.05.19',
        'updated_at' => '2022.05.19',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 19,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2019.04.22',
        'updated_at' => '2019.04.22',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 20,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2021.04.19',
        'updated_at' => '2021.04.19',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 21,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::PATIENT,
        'created_at' => '2022.05.19',
        'updated_at' => '2022.05.19',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 22,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::LABORATORY_ASSISTANT,
        'created_at' => '2021.04.19',
        'updated_at' => '2021.04.19',
        'username' => 'laboratory_assistant',
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
    [
        'id' => 23,
        'auth_key' => Yii::$app->security->generateRandomString(),
        'password_hash' => Yii::$app->security->generatePasswordHash('1234'),
        'role' => RoleTypes::LABORATORY_ASSISTANT,
        'created_at' => '2022.05.19',
        'updated_at' => '2022.05.19',
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ],
];
