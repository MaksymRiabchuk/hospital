<?php

use common\models\enums\DepartmentFloors;

return [
    [
        'id' => 1,
        'name' => 'Травматологічне',
        'address' => DepartmentFloors::FIRST_FLOOR,
        'is_deleted' => 0,
    ],
    [
        'id' => 2,
        'name' => 'Кож-Венерелогічне',
        'address' => DepartmentFloors::SECOND_FLOOR,
        'is_deleted' => 0,
    ],
    [
        'id' => 3,
        'name' => 'Хірургічне',
        'address' => DepartmentFloors::THIRD_FLOOR,
        'is_deleted' => 0,
    ],
    [
        'id' => 4,
        'name' => 'Терапевтичне',
        'address' => DepartmentFloors::FOURTH_FLOOR,
        'is_deleted' => 0,
    ],
    [
        'id' => 5,
        'name' => 'Лабораторія',
        'address' => DepartmentFloors::FIFTH_FLOOR,
        'is_deleted' => 0,
    ],
];
