<?php /** @noinspection PhpUnhandledExceptionInspection */


use Faker\Factory;

/**
 * @throws Exception
 */
$faker= Factory::create();
return [
    [
        'id' => 1,
        'pre_record_procedure_id' => 1,
        'file' => '/uploads/laboratoryFiles3.jpeg',
        'name' => $faker->text(10),
    ],
    [
        'id' => 2,
        'pre_record_procedure_id' => 1,
        'file' => '/uploads/laboratoryFiles4.jpeg',
        'name' => $faker->text(10),
    ],
    [
        'id' => 3,
        'pre_record_procedure_id' => 2,
        'file' => '/uploads/laboratoryFiles5.jpeg',
        'name' => $faker->text(10),
    ],
    [
        'id' => 4,
        'pre_record_procedure_id' => 2,
        'file' => '/uploads/laboratoryFiles6.jpeg',
        'name' => $faker->text(10),
    ],
    [
        'id' => 5,
        'pre_record_procedure_id' => 3,
        'file' => '/uploads/laboratoryFiles7.jpeg',
        'name' => $faker->text(10),
    ],
    [
        'id' => 6,
        'pre_record_procedure_id' => 3,
        'file' => '/uploads/laboratoryFiles8.jpeg',
        'name' => $faker->text(10),
    ],
    [
        'id' => 7,
        'pre_record_procedure_id' => 4,
        'file' => '/uploads/laboratoryFiles9.jpeg',
        'name' => $faker->text(10),
    ],
];
