<?php /** @noinspection PhpUnhandledExceptionInspection */


use common\models\enums\PreRecordsStatusTypes;

/**
 * @throws Exception
 */
function getRandomVisitDate(): int
{
    $dateTime = new DateTime(date('Y-m-d H:00',time()));
    $interval = new DateInterval('P0Y0DT0H' . rand(1, 11) * 15 . 'M');
    $dateTime->add($interval);
    return $dateTime->getTimestamp();
}

return [
    [
        'id' => 1,
        'doctor_id' => 1,
        'patient_id' => 1,
        'visit_date' => getRandomVisitDate(),
        'status' =>PreRecordsStatusTypes::IN_WAIT,
        'procedure_type_id'=>1,
    ],
    [
        'id' => 2,
        'doctor_id' => 1,
        'patient_id' => 2,
        'visit_date' => getRandomVisitDate(),
        'status' =>PreRecordsStatusTypes::IN_WAIT,
        'procedure_type_id'=>2,
    ],
    [
        'id' => 3,
        'doctor_id' => 1,
        'patient_id' => 3,
        'visit_date' => getRandomVisitDate(),
        'status' =>PreRecordsStatusTypes::IN_WAIT,
        'procedure_type_id'=>3,
    ],
    [
        'id' => 4,
        'doctor_id' => 2,
        'patient_id' => 4,
        'visit_date' => getRandomVisitDate(),
        'status' =>PreRecordsStatusTypes::IN_WAIT,
        'procedure_type_id'=>1,
    ],
    [
        'id' => 5,
        'doctor_id' => 2,
        'patient_id' => 5,
        'visit_date' => getRandomVisitDate(),
        'status' =>PreRecordsStatusTypes::IN_WAIT,
        'procedure_type_id'=>2,
    ],
    [
        'id' => 6,
        'doctor_id' => 2,
        'patient_id' => 6,
        'visit_date' => getRandomVisitDate(),
        'status' =>PreRecordsStatusTypes::IN_WAIT,
        'procedure_type_id'=>3,
    ],
    [
        'id' => 7,
        'doctor_id' => 3,
        'patient_id' => 7,
        'visit_date' => getRandomVisitDate(),
        'status' =>PreRecordsStatusTypes::IN_WAIT,
        'procedure_type_id'=>1,
    ],
    [
        'id' => 8,
        'doctor_id' => 3,
        'patient_id' => 8,
        'visit_date' => getRandomVisitDate(),
        'status' =>PreRecordsStatusTypes::IN_WAIT,
        'procedure_type_id'=>2,
    ],
    [
        'id' => 9,
        'doctor_id' => 3,
        'patient_id' => 9,
        'visit_date' => getRandomVisitDate(),
        'status' =>PreRecordsStatusTypes::IN_WAIT,
        'procedure_type_id'=>3,
    ],
];
