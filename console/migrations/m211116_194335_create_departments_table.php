<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%department}}`.
 */
class m211116_194335_create_departments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%departments}}', [
            'id' => $this->primaryKey(),
            'name'=> $this->string()->notNull(),
            'address'=> $this->string()->notNull(),
            'is_deleted'=> $this->boolean()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%departments}}');
    }
}
