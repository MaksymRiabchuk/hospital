<?php

use yii\db\Migration;

/**
 * Class m221228_105222_alter_pre_records_procedures_table
 */
class m221228_105222_alter_pre_records_procedures_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%pre_records_procedures}}','patient_card_history_id',$this->integer());
        $this->addForeignKey('fk-pre_records_procedures-patient_card_history_id','{{%pre_records_procedures}}',
            'patient_card_history_id','{{%patient_card_history}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-pre_records_procedures-patient_card_history_id','{{%pre_records_procedures}}');
        $this->dropColumn('{{%pre_records_procedures}}','patient_card_history_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221228_105222_alter_pre_records_procedures_table cannot be reverted.\n";

        return false;
    }
    */
}
