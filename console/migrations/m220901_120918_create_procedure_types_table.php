<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%procedure_types}}`.
 */
class m220901_120918_create_procedure_types_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%procedure_types}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->notNull()->comment('Назва'),
            'description'=>$this->text()->comment('Додатковий опис')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%procedure_types}}');
    }
}
