<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%doctor_visits}}`.
 */
class m221228_210324_drop_doctor_visits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%doctor_visits}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%doctor_visits}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}
