<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%patient_card_history}}`.
 */
class m220831_171048_create_patient_card_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%patient_card_history}}', [
            'id' => $this->primaryKey(),
            'visit_date'=>$this->integer()->notNull()->comment('Дата візиту'),
            'object_type'=>$this->smallInteger()->comment('Тип обʼєкта'),
            'object_id'=>$this->integer()->comment('ID обʼєкта'),
            'patient_id'=>$this->integer()->notNull()->comment('Пацієнт'),
            'description'=>$this->text()->notNull()->comment('Опис'),
        ]);
        $this->addForeignKey('fk-patient_card_history-patient_id-patient-id','{{%patient_card_history}}','patient_id',
            '{{%patients}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-patient_card_history-patient_id-patient-id','{{%patient_card_history}}');
        $this->dropTable('{{%patient_card_history}}');
    }
}
