<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%procedure_visits_attachments}}`.
 */
class m221101_184016_create_procedure_visits_attachments_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%procedure_visits_attachments}}', [
            'id' => $this->primaryKey(),
            'procedure_visit_id'=>$this->integer()->notNull(),
            'file'=>$this->string()->notNull(),
        ]);
        $this->addForeignKey('fk-procedure_visits_attachments-procedure_visit_id-procedures-id',
            '{{%procedure_visits_attachments}}',
            'procedure_visit_id',
            '{{%procedure_visits}}',
            'id');
    }
//
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-procedure_visits_attachments-procedure_visit_id-procedures-id','{{%procedure_visits_attachments}}');
        $this->dropTable('{{%procedure_visits_attachments}}');
    }
}
