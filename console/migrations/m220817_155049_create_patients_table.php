<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%patient}}`.
 */
class m220817_155049_create_patients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%patients}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Імʼя'),
            'surname'=> $this->string()->notNull()->comment('Прізвище'),
            'parent_name'=> $this->string()->notNull()->comment('По батькові'),
            'image'=>$this->string()->comment('Фото'),
            'user_id'=>$this->integer(),
        ]);
        $this->addForeignKey('FK-patient-user_id-user-id','{{%patients}}','user_id','{{%user}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-patient-user_id-user-id','{{%patients}}');
        $this->dropTable('{{%patients}}');
    }
}
