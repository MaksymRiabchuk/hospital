<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%doctor_visits}}`.
 */
class m221101_183150_create_doctor_visits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%doctor_visits}}', [
            'id' => $this->primaryKey(),
            'patient_id'=>$this->integer()->notNull()->comment('Пацієнт'),
            'doctor_id'=>$this->integer()->notNull()->comment('Лікар'),
            'visit_date'=>$this->integer()->notNull()->comment('Дата візиту'),
            'description'=>$this->text()->notNull()->comment('Нотатки'),
        ]);
        $this->addForeignKey('fk-doctor_visits-patient_id-patients-id','{{%doctor_visits}}','patient_id',
        '{{%patients}}','id');
        $this->addForeignKey('fk-doctor_visits-doctor_id-doctors-id','{{%doctor_visits}}','doctor_id',
        '{{%doctors}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-doctor_visits-patient_id-patients-id','{{%doctor_visits}}');
        $this->dropForeignKey('fk-doctor_visits-doctor_id-doctors-id','{{%doctor_visits}}');
        $this->dropTable('{{%doctor_visits}}');
    }
}
