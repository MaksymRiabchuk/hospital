<?php

use common\models\enums\PreRecordsStatusTypes;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%pre_records_doctors}}`.
 */
class m220817_161421_create_pre_records_doctors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pre_records_doctors}}', [
            'id' => $this->primaryKey(),
            'doctor_id'=> $this->integer()->comment('Лікар'),
            'user_id' =>$this->integer()->comment('Пацієнт'),
            'visit_date'=>$this->integer()->notNull()->comment('Дата візиту'),
            'status'=>$this->integer()->notNull()->defaultValue(PreRecordsStatusTypes::IN_WAIT)->comment('Статус'),
        ]);
        $this->addForeignKey('fk-pre_records_doctors-doctor_id-doctors-id','{{%pre_records_doctors}}','doctor_id',
            '{{%doctors}}','id');
        $this->addForeignKey('fk-pre_records_doctors-user_id-user-id','{{%pre_records_doctors}}','user_id',
            '{{%user}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-pre_records_doctors-doctor_id-doctors-id','{{%pre_records_doctors}}');
        $this->dropForeignKey('fk-pre_records_doctors-user_id-user-id','{{%pre_records_doctors}}');
        $this->dropTable('{{%pre_records_doctors}}');
    }
}