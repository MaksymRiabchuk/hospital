<?php

use yii\db\Migration;

/**
 * Class m221228_111632_alter_patient_card_history_table
 */
class m221228_111632_alter_patient_card_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%patient_card_history}}','description',$this->text()->comment('Нотатки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%patient_card_history}}','description');
        $this->addColumn('{{%patient_card_history}}',
            'description',$this->text()->notNull()->comment('Опис'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221228_111632_alter_patient_card_history_table cannot be reverted.\n";

        return false;
    }
    */
}
