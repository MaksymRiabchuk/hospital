<?php

use common\models\enums\PreRecordsStatusTypes;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%pre_records_procedures}}`.
 */
class m220901_120952_create_pre_records_procedures_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pre_records_procedures}}', [
            'id' => $this->primaryKey(),
            'patient_id'=>$this->integer()->comment('Пацієнт'),
            'doctor_id'=>$this->integer()->comment('Лікар'),
            'visit_date'=>$this->integer()->comment('Дата'),
            'procedure_type_id'=>$this->integer()->comment('Назва процедури'),
            'status'=>$this->integer()->defaultValue(PreRecordsStatusTypes::IN_WAIT)->comment('Статус'),
        ]);
        $this->addForeignKey('fk-pre_records_procedures-patient_id-patients-id','{{%pre_records_procedures}}','patient_id',
            '{{%patients}}','id');
        $this->addForeignKey('fk-pre_records_procedures-doctor_id-doctors-id','{{%pre_records_procedures}}','doctor_id',
            '{{%doctors}}','id');
        $this->addForeignKey('fk-pre_records_procedures-procedure_type_id-procedures_types-id','{{%pre_records_procedures}}',
            'procedure_type_id',
            '{{%procedure_types}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-pre_records_procedures-patient_id-patients-id','{{%pre_records_procedures}}');
        $this->dropForeignKey('fk-pre_records_procedures-doctor_id-doctors-id','{{%pre_records_procedures}}');
        $this->dropForeignKey('fk-pre_records_procedures-procedure_type_id-procedures_types-id','{{%pre_records_procedures}}');
        $this->dropTable('{{%pre_records_procedures}}');
    }
}
