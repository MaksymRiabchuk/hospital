<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%procedure_visits}}`.
 */
class m221228_210420_drop_procedure_visits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%procedure_visits}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%procedure_visits}}', [
            'id' => $this->primaryKey(),
        ]);
    }
}
