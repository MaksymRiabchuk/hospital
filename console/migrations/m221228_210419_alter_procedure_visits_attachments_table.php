<?php

use yii\db\Migration;

/**
 * Class m221228_210846_alter_procedure_visits_attachments_table
 */
class m221228_210419_alter_procedure_visits_attachments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%procedure_visits_attachments}}');
        $this->createTable('{{%pre_records_procedures_attachments}}',[
            'id'=>$this->primaryKey(),
            'pre_record_procedure_id'=>$this->integer(),
            'file'=>$this->string()->comment('Вкладення'),
        ]);
        $this->addForeignKey('fk-pre_records_procedures_attachments','{{%pre_records_procedures_attachments}}',
            'pre_record_procedure_id','{{%pre_records_procedures}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pre_records_procedures_attachments}}');
        $this->createTable('{{%procedure_visits_attachments}}',[
            'id'=>$this->primaryKey(),
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221228_210846_alter_procedure_visits_attechments_table cannot be reverted.\n";

        return false;
    }
    */
}
