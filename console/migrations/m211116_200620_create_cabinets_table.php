<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cabinets}}`.
 */
class m211116_200620_create_cabinets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cabinets}}', [
            'id' => $this->primaryKey(),
            'name'=> $this->string()->notNull(),
            'description'=> $this->text(),
            'is_deleted'=> $this->boolean()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cabinets}}');
    }
}
