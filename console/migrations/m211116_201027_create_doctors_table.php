<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%doctors}}`.
 */
class m211116_201027_create_doctors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%doctors}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'surname'=> $this->string(),
            'parent_name'=> $this->string(),
            'specialization_id'=> $this->integer(),
            'image'=>$this->string(),
            'user_id'=>$this->integer(),
        ]);
        $this->addForeignKey('FK-doctors-specialization_id-specializations-id','{{%doctors}}','{{%specialization_id}}','{{%specializations}}','{{%id}}');
        $this->addForeignKey('FK-doctors-user_id-user-id','{{%doctors}}','{{%user_id}}','{{%user}}','{{%id}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-doctors-user_id-user-id','{{%doctors}}');
        $this->dropForeignKey('FK-doctors-specialization_id-specializations-id','{{%doctors}}');
        $this->dropTable('{{%doctors}}');
    }
}
