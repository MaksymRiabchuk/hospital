<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%laboratory_assistants}}`.
 */
class m221025_122336_create_laboratory_assistants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%laboratory_assistants}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'surname'=> $this->string(),
            'parent_name'=> $this->string(),
            'image'=>$this->string(),
            'user_id'=>$this->integer(),
        ]);
        $this->addForeignKey('FK-laboratory_assistants-user_id-user-id','{{%laboratory_assistants}}','{{%user_id}}',
            '{{%user}}','{{%id}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('FK-laboratory_assistants-user_id-user-id','{{%laboratory_assistants}}');
        $this->dropTable('{{%laboratory_assistants}}');

    }
}
