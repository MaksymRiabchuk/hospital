<?php

use common\models\enums\ProcedureVisitStatus;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%procedure_visits}}`.
 */
class m221101_184013_create_procedure_visits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%procedure_visits}}', [
            'id' => $this->primaryKey(),
            'patient_id'=>$this->integer()->notNull()->comment('Пацієнт'),
            'laboratory_assistant_id'=>$this->integer()->notNull()->comment('Лаборант'),
            'procedure_date'=>$this->integer()->notNull()->comment('Дата процедури'),
            'procedure_type_id'=>$this->integer()->notNull()->comment('Тип процедури'),
            'description'=>$this->text()->notNull()->comment('Нотатки'),
            'status'=>$this->smallInteger()->notNull()->defaultValue(ProcedureVisitStatus::DIRECTED)->comment('Статус')
        ]);
        $this->addForeignKey('fk-procedure_visits-patient_id-patients-id','{{%procedure_visits}}','patient_id',
        '{{patients}}','id');
        $this->addForeignKey('fk-procedure_visits-lab_assistant_id-lab_assistants-id','{{%procedure_visits}}',
            'laboratory_assistant_id','{{%laboratory_assistants}}','id');
        $this->addForeignKey('fk-procedure_visits-procedure_type_id-procedure_types-id','{{%procedure_visits}}',
            'procedure_type_id','{{%procedure_types}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-procedure_visits-patient_id-patients-id','{{%procedure_visits}}');
        $this->dropForeignKey('fk-procedure_visits-lab_assistant_id-lab_assistants-id','{{%procedure_visits}}');
        $this->dropForeignKey('fk-procedure_visits-procedure_type_id-procedure_types-id','{{%procedure_visits}}');
        $this->dropTable('{{%procedure_visits}}');
    }
}
