<?php

use yii\db\Migration;

/**
 * Class m230101_172124_alter_pre_records_procedures_attachments_table
 */
class m230101_172124_alter_pre_records_procedures_attachments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%pre_records_procedures_attachments}}','name',
            $this->string()->notNull()->comment('Назва файлу'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pre_records_procedures_attachments}}','name');
    }
}
