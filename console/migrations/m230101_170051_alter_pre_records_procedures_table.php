<?php

use yii\db\Migration;

/**
 * Class m230101_170051_alter_pre_records_procedures_table
 */
class m230101_170051_alter_pre_records_procedures_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%pre_records_procedures}}','description',$this->text()->comment('Примітки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pre_records_procedures}}','description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230101_170051_alter_pre_records_procedures_table cannot be reverted.\n";

        return false;
    }
    */
}
