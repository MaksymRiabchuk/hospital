<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%specializations}}`.
 */
class m211116_195641_create_specializations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%specializations}}', [
            'id' => $this->primaryKey(),
            'name'=> $this->string()->notNull(),
            'department_id'=> $this->integer()->notNull(),
            'is_deleted'=> $this->boolean()->defaultValue(0),
        ]);
        $this->addForeignKey('FK-specializations-department_id-departments-id','{{%specializations}}','{{%department_id}}','{{%departments}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK-specializations-department_id-departments-id','{{%specializations}}');
        $this->dropTable('{{%specializations}}');
    }
}
