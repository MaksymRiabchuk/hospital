<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class GrantsController extends Controller
{

    private $auth;

    private $grants = [
        ['rule' => 'admin', 'description' => 'Адміністратор'],

        ['rule' => 'doctor-profile', 'description' => 'Доступ до кабінету лікаря'],
        ['rule' => 'doctor-records-index', 'description' => 'Доступ до записів лікаря'],
        ['rule' => 'change-doctor-password', 'description' => 'Доступ до зміни паролю лікаря'],

        ['rule' => 'patient-profile', 'description' => 'Доступ до профілю пацієнта'],
        ['rule' => 'change-patient-password', 'description' => 'Доступ до зміни паролю пацієнта'],
        ['rule' => 'create-record', 'description' => 'Створити запис до лікаря'],
        ['rule' => 'view-patient-card-history', 'description' => 'Доступ до перегляду карти пацієнта'],


        ['rule' => 'laboratory-assistant-profile', 'description' => 'Доступ до профілю лаборанта'],
        ['rule' => 'laboratoryʼs-records', 'description' => 'Доступ до записів в лабораторію'],
        ['rule' => 'change-laboratory-assistant-password', 'description' => 'Доступ до зміни паролю лаборанта'],
    ];

    public function addGrant($name, $description,$addAdmin)
    {
        $permission = $this->auth->createPermission('p_' . $name);
        $permission->description = $description;
        $this->auth->add($permission);
        $role = $this->auth->createRole($name);
        $role->description = $description;
        $this->auth->add($role);
        $this->auth->addChild($role, $permission);
        Yii::$app->authManager->assign($role,1);
    }

    public function actionIndex($addAdmin=true)
    {
        $this->auth = Yii::$app->authManager;
        $this->auth->removeAll();
        foreach ($this->grants as $item) {
            $this->addGrant($item['rule'],$item['description'],$addAdmin);
        }
    }

    public function actionFillGrant(){
        $this->auth = Yii::$app->authManager;
        $arrayRolesDoctor=['change-doctor-password','doctor-profile','doctor-records-index'];
        $arrayRolesPatient=['change-patient-password','patient-profile','create-record','view-patient-card-history'];
        $arrayRolesLaboratoryAssistant=['laboratory-assistant-profile','laboratoryʼs-records','change-laboratory-assistant-password'];

        foreach ($arrayRolesDoctor as $role) {
            $new_role = Yii::$app->authManager->getRole($role);
            Yii::$app->authManager->assign($new_role, '2');
        }

        foreach ($arrayRolesPatient as $role) {
            $new_role = Yii::$app->authManager->getRole($role);
            Yii::$app->authManager->assign($new_role, '12');
        }

        foreach ($arrayRolesLaboratoryAssistant as $role) {
            $new_role = Yii::$app->authManager->getRole($role);
            Yii::$app->authManager->assign($new_role, '22');
        }
    }
}