<?php use common\models\entity\PreRecordsProceduresAttachments;
use kartik\file\FileInput;
use yii\bootstrap\Html;
use yii\bootstrap4\ActiveForm;
/* @var $model PreRecordsProceduresAttachments */
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
<div class="card card-default">
    <div class="card card-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'imageFile')->widget(FileInput::class,
                    [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'initialPreview' => (!$model->file) ? false : $model->file,
                            'initialPreviewAsData' => true,
                            'showPreview' => true,
                            'showRemove' => false,
                            'showUpload' => false
                        ],
                    ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput()?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= Html::submitButton(Yii::t('app', 'Зберегти'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
