<?php

use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\entity\PreRecordsProcedures;
use common\models\entity\ProcedureTypes;
use common\models\enums\ProcedureVisitStatus;
use kartik\file\FileInput;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $model PreRecordsProcedures */
?>
<div class="procedure-form">


    <h1><?= Html::encode($this->title) ?></h1>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'patient_id',
                'format' => 'raw',
                'value' => function ($data) {
                    /* @var $data PreRecordsProcedures */
                    return isset($data->patient_id) ?
                        $data->patient->surname . ' ' . $data->patient->name . ' ' . $data->patient->parent_name :
                        'Такого пацієнта не існує';
                }
            ],
            [
                'attribute' => 'doctor_id',
                'format' => 'raw',
                'filter' => Doctor::getDoctorsList(),
                'value' => function ($data) {
                    /* @var $data PreRecordsProcedures */
                    return isset($data->doctor_id) ? $data->doctor->surname . ' ' . $data->doctor->name : 'Такого лікаря не існує';
                }
            ],
            [
                'attribute' => 'visit_date',
                'format' => ['datetime', 'php:d.m.Y'],
            ],
            [
                'attribute' => 'procedure_type_id',
                'format' => 'raw',
                'filter' => ProcedureTypes::getProcedureTypesById(),
                'value' => function ($data) {
                    /* @var $data PreRecordsProcedures */
                    return isset($data->procedure_type_id) ? $data->procedureType->name : 'Такого аналізу не існує';
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => ProcedureVisitStatus::listData(),
                'value' => function ($data) {
                    /* @var PreRecordsProcedures $data */
                    return isset($data->status) ? ProcedureVisitStatus::getLabel($data->status) : 'Такого статусу не існує';
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Дії',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        if (in_array($model->status,[ProcedureVisitStatus::DIRECTED,ProcedureVisitStatus::IN_PROCESS])) {
                            return Html::a(
                                '<span class="btn btn-primary " aria-hidden="true">Редагувати</span>',
                                Url::to(['procedure/update','record_id'=>$key]));
                        } else {
                            return '';
                        }
                    }
                ]
            ],
        ],
    ]); ?>

</div>