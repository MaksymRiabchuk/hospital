<?php

use common\models\enums\ProcedureVisitStatus;
use kartik\file\FileInput;
use modules\laboratory\models\forms\EditPreRecordProcedureForm;
use yii\bootstrap\Html;
use yii\bootstrap4\Modal;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model EditPreRecordProcedureForm */
?>
<div class="procedure-form">


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="card card-default">
        <div class="card card-body">
            <div class="row">
                <div class="col-md-4">
                    <div style="margin-bottom: 10px">
                        <?= Html::a('Добавити вкладення', ['procedure/add-attachment', 'record_id' => $model->record_id], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div>
                        <strong>
                            Список вкладень
                        </strong>
                    </div>
                    <table class="table" style="margin-top: 10px">
                        <thead>
                        <tr>
                            <th>
                                Вкладення
                            </th>
                            <th>
                                Назва файлу
                            </th>
                            <th>
                                Дії
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($model->attachments as $item): ?>
                            <tr>
                                <td>
                                    <?=Html::img($item->file,['style'=>'max-height:50px'])?>
                                </td>
                                <td>
                                    <?=$item->name?>
                                </td>
                                <td>
                                    <?= Html::a('X',['procedure/delete-attachment','attachment_id'=>$item->id,'record_id'=>$model->record_id],
                                        ['class'=>'btn btn-danger'])?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>

                    </table>

                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'patientFullName')->textInput(['readOnly' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'doctorFullName')->textInput(['readOnly' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'visit_date')->textInput(['readOnly' => true]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'status')->dropDownList([
                                ProcedureVisitStatus::IN_PROCESS => ProcedureVisitStatus::getLabel(ProcedureVisitStatus::IN_PROCESS),
                                ProcedureVisitStatus::PROCESSED => ProcedureVisitStatus::getLabel(ProcedureVisitStatus::PROCESSED),
                                ProcedureVisitStatus::DECLINE => ProcedureVisitStatus::getLabel(ProcedureVisitStatus::DECLINE),
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'procedure_type_id')->textInput(['readOnly' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'description')->textarea() ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::submitButton(Yii::t('app', 'Зберегти'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
