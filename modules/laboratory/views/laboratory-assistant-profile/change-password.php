<?php
/* @var $model ChangePasswordForm /*/

use common\models\forms\ChangePasswordForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form= ActiveForm::begin()?>
<div class="row">
    <div class="col-md-4">
        <?=$form->field($model,'old_password')->textInput();?>
    </div>
    <div class="col-md-4">
        <?=$form->field($model,'new_password')->textInput();?>
    </div>
    <div class="col-md-4">
        <?=$form->field($model,'confirm_password')->textInput();?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= Html::submitButton(Yii::t('app','Зберегти'), ['class' => 'btn btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end()?>
