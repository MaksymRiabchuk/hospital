<?php

namespace modules\laboratory\controllers;

use common\models\forms\ChangePasswordForm;
use modules\laboratory\models\forms\ChangeLaboratoryAssistantProfileForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class LaboratoryAssistantProfileController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            'roles' => ['laboratory-assistant-profile'],
                        ],
                        [
                            'actions' => ['change-password'],
                            'allow' => true,
                            'roles' => ['change-laboratory-assistant-password'],
                        ],
                    ],
                ],
            ]
        );
    }
    public function actionIndex()
    {
        $model = new ChangeLaboratoryAssistantProfileForm();
        if ($model->load($this->request->post())) {
            $model->save();
            Yii::$app->session->addFlash('success', Yii::t('app','Успішно змінено'));
            return $this->redirect('index');
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }
    public function actionChangePassword()
    {
        $model=new ChangePasswordForm();
        if($model->load($this->request->post()) &&  $model->validate()){
            $model->changePassword();
            Yii::$app->session->addFlash('success', Yii::t('app','Успішно змінено'));
            return $this->redirect('index');
        }
        return $this->render('change-password', [
            'model' => $model,
        ]);
    }
}
