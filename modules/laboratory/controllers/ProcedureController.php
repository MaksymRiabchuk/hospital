<?php

namespace modules\laboratory\controllers;

use common\models\classes\FormattingRecord;
use common\models\classes\FormattingSchedule;
use common\models\entity\Doctor;
use common\models\entity\HospitalSchedule;
use common\models\entity\Patient;
use common\models\entity\PatientCardHistory;
use common\models\entity\PreRecordsProcedures;
use common\models\entity\PreRecordsProceduresAttachments;
use common\models\entity\Procedure;
use common\models\entity\ScheduleRecordsDoctors;
use common\models\enums\ObjectTypesInPatientCard;
use common\models\enums\PreRecordsStatusTypes;
use common\models\enums\ProcedureVisitStatus;
use common\models\search\HospitalScheduleSearch;
use common\models\search\PatientSearch;
use common\models\search\PreRecordsProceduresSearch;
use common\models\search\ProcedureSearch;
use DateTime;
use modules\doctorprofile\models\classes\DoctorRecords;
use modules\laboratory\models\classes\SaveProcedure;
use modules\laboratory\models\forms\EditPreRecordProcedureForm;
use Symfony\Component\String\Inflector\FrenchInflector;
use Yii;
use yii\base\InvalidArgumentException;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class ProcedureController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => ['index','update','add-record-to-patient-history','add-attachment','delete-attachment'],
                            'allow' => true,
                            'roles' => ['laboratoryʼs-records'],
                        ],
                    ],
                ],
            ]
        );
    }

    public function actionIndex()
    {
        $this->changeStatusPreviousDayRecords();
        $searchModel = new PreRecordsProceduresSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel,
        ]);
    }

    private function changeStatusPreviousDayRecords(){
        $records=PreRecordsProcedures::find()->where(['<=','visit_date',strtotime(date('d.m.y 00:00'))])
            ->andWhere(['status'=>ProcedureVisitStatus::DIRECTED])->all();
        if($records){
            foreach ($records as $record){
                /* @var $record PreRecordsProcedures */
                $record->status=ProcedureVisitStatus::DECLINE;
                $record->save();
            }
        }
    }

    public function actionAddRecordToPatientHistory($record_id){
        $model=new PatientCardHistory();
        $record=ScheduleRecordsDoctors::find()->where(['id'=>$record_id])->one();
        if($record){
            $patient=Patient::find()->where(['user_id'=>$record->user_id])->one();
            $model->patient_id=$patient->id;
            $model->doctor_id=$record->doctor_id;
            $model->visit_date=$record->created_at;
        }
        if ($model->load($this->request->post())) {
            if($model->save()){
                return $this->render('successfully-added-record-to-patient-history');
            }
            else{
                echo '<br>';
                echo '<br>';
                echo '<br>';
                echo '<br>';
                echo '<br>';
                echo '<br>';
                var_dump($model->errors);
            }

        }
        return $this->render('add-record-to-patient-history',[
            'model'=>$model,
            'patient'=>$patient,
        ]);

    }

    /**
     * Updates an existing HospitalSchedule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $record_id
     * @return string|Response
     * @throws Exception|\yii\base\InvalidConfigException
     */
    public function actionUpdate($record_id)
    {
        $model=new EditPreRecordProcedureForm($record_id);
        if ($model->load(Yii::$app->request->post())) {
            if($model->status==ProcedureVisitStatus::PROCESSED){
                $this->createRecordInPatientCardHistory($model->patient->id,$model->visit_date,
                    ObjectTypesInPatientCard::LABORATORY_ASSISTANT,$model->laboratory_assistant_id,$model->description);
            }
            if($model->saveProcedure()){
                return $this->redirect('index');
            }
            else{
                throw new Exception('Помилка збереження');
            }
        }
        return $this->render('update', [
            'model'=>$model,
        ]);
    }

    public function createRecordInPatientCardHistory($patient_id,$visit_date,$object_type,$object_id,$description): bool
    {
        $model=new PatientCardHistory();
        $model->patient_id=$patient_id;
        $model->visit_date=strtotime($visit_date);
        $model->object_type=$object_type;
        $model->object_id=$object_id;
        $model->description=$description;
        if($model->save()){
            return true;
        }
        else{
            dump($model->errors);
            die();
            return false;
        }

    }

    /**
     * @throws Exception|InvalidArgumentException
     */
    public function actionAddAttachment($record_id){
        $model=new PreRecordsProceduresAttachments();
        if ($model->load(Yii::$app->request->post())) {
            if($model->saveAttachment($record_id)){
                return $this->redirect(['update','record_id'=>$record_id]);
            }
            else{
                throw new Exception('Помилка збереження');
            }
        }
        return $this->render('add-attachments',[
            'model'=>$model,
        ]);
    }

    public function actionDeleteAttachment($attachment_id,$record_id){
        $attachment=PreRecordsProceduresAttachments::findOne($attachment_id);
        if($attachment){
            $attachment->delete();
            return $this->redirect(['update',
                'record_id'=>$record_id,
            ]);
        }
        else{
            throw new  Exception('Такого вкладення не існує');
        }

    }

}
