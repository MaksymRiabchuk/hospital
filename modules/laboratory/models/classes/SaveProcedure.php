<?php
//
//namespace modules\laboratory\models\classes;
//
//use common\models\entity\Doctor;
//use common\models\entity\Patient;
//use common\models\entity\Procedure;
//use common\models\entity\ProcedureAttachment;
//use common\models\enums\ProcedureStatus;
//use http\Url;
//use Yii;
//use yii\base\Model;
//use yii\db\ActiveRecord;
//use yii\widgets\ActiveForm;
//
//class SaveProcedure extends Model
//{
//    public $attachments = [];
//    public $patient_id;
//    private $arrayFiles;
//    public $doctor_id;
//    public $procedure_id;
//    public $procedure_date;
//    public $description;
//    public $status;
//
//    public function rules()
//    {
//        return [
//            [['patient_id', 'doctor_id', 'procedure_id','status'], 'integer'],
//            [['description'], 'string'],
//            [['procedure_date', 'files'], 'safe'],
//            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_id' => 'id']],
//            [['patient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Patient::className(), 'targetAttribute' => ['patient_id' => 'id']],
//        ];
//    }
//
//    public function __constructor($id=null,$config=null)
//    {
//        if(!$id==null){
//            $attachments=ProcedureAttachment::find()->where(['procedure_id'=>$id]);
//        }
//        else{
//            $attachments=[];
//        }
//    }
//
//    public function saveProcedure()
//    {
//        $transaction = Yii::$app->db->beginTransaction();
//        $model = new Procedure();
//        $model->doctor_id = $this->doctor_id;
//        $model->patient_id = $this->patient_id;
//        $model->procedure_date = $this->procedure_date;
//        $model->procedure_id = $this->procedure_id;
//        $model->description = $this->description;
//        if ($model->errors) {
//            $transaction->rollBack();
//        }
//        $model->status=$this->status;
//        $model->save();
//        $transaction->commit();
//        return $model->id;
//    }
//    public function saveProcedureAttachment($procedure_id){
//        $model=new ProcedureAttachment();
//        isset($_FILES['SaveProcedure']) ? $this->arrayFiles = $_FILES['SaveProcedure'] : $this->arrayFiles = null;
////        var_dump($this->arrayFiles);
//        if(!$this->arrayFiles===null){
//            $this->setFileNames();
//            $this->setFileSize();
//            $this->setTmpName();
//            $this->setTypeFile();
//            $this->setErrorFiles();
//            $path=dirname(dirname(dirname(dirname(__DIR__)))) . '/backend/web/uploads/';
//
//            foreach ($this->attachments as $key=>$item) {
//                move_uploaded_file($item['tmp_name'], $path.$item['name']);
//                $model->procedure_id=$procedure_id;
//                $model->file=$path.$item['name'];
//                if($model->errors){
//                }
//                $model->save();
//
//            }
//        }
//        $model->save();
////        if ($model->errors) {
////            $transaction->rollBack();
////        }
//
//    }
////    public function findP
//    private function setFileNames(){
//        foreach ($this->arrayFiles['name']['attachments'] as $key=>$item){
//            $this->attachments[$key]['name']=$item;
//        }
//    }
//    private function setFileSize(){
//        foreach ($this->arrayFiles['size']['attachments'] as $key=>$item){
//            $this->attachments[$key]['size']=$item;
//        }
//    }
//    private function setTmpName(){
//        foreach ($this->arrayFiles['tmp_name']['attachments'] as $key=>$item){
//            $this->attachments[$key]['tmp_name']=$item;
//        }
//    }
//    private function setTypeFile(){
//        foreach ($this->arrayFiles['type']['attachments'] as $key=>$item){
//            $this->attachments[$key]['type']=$item;
//        }
//    }
//    private function setErrorFiles(){
//        foreach ($this->arrayFiles['error']['attachments'] as $key=>$item){
//            $this->attachments[$key]['error']=$item;
//        }
//    }
//}