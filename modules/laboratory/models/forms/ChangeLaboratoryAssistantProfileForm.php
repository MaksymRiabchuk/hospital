<?php

namespace modules\laboratory\models\forms;


use common\models\entity\Doctor;
use common\models\entity\LaboratoryAssistant;
use common\models\entity\Patient;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\web\UploadedFile;

class ChangeLaboratoryAssistantProfileForm extends Model
{
    public $surname;
    public $name;
    public $parent_name;
    public $image;
    public $imageFile;

    public $username;
    public $phone;
    public $email;

    public function rules()
    {
        return [
            [['surname', 'name', 'parent_name', 'phone'], 'required'],
            [['imageFile'], 'string'],
        ];
    }

    public function __construct($config = [])
    {
        parent::__construct($config);

        if (Yii::$app->user->isGuest) {
            throw new Exception('Змінювати профіль можуть лише авторизовані лаборанти');
        }

        $user = Yii::$app->user->identity;
        /* @var $user User */
        $this->username = $user->username;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->surname = $user->laboratoryAssistant->surname;
        $this->name = $user->laboratoryAssistant->name;
        $this->parent_name = $user->laboratoryAssistant->parent_name;
        $this->image = $user->laboratoryAssistant->image;
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Нікнейм'),
            'phone' => Yii::t('app', 'Телефон'),
            'email' => Yii::t('app', 'Ел. пошта'),
            'name' => Yii::t('app', 'Ім`я'),
            'surname' => Yii::t('app', 'Прізвище'),
            'parent_name' => Yii::t('app', 'По батькові'),
            'imageFile' => Yii::t('app', 'Фото'),
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            throw new Exception('Помилка збереження');
        }
        $transaction=Yii::$app->db->beginTransaction();
        $user = Yii::$app->user->identity;
        $user->phone = $this->phone;
        $laboratoryAssistant=LaboratoryAssistant::find()->where(['user_id'=>$user->id])->one();
        /* @var $laboratoryAssistant LaboratoryAssistant */
        $laboratoryAssistant->name=$this->name;
        $laboratoryAssistant->surname=$this->surname;
        $laboratoryAssistant->parent_name=$this->parent_name;
        if (!$this->image) {
            $this->image = '';
        }
        $image = UploadedFile::getInstance($this, 'imageFile');
        if ($image) {
            $res = $image->saveAs('uploads/laboratoryAssistants/laboratoryAssistantImage' . $laboratoryAssistant->id . '.' . $image->getExtension());
            $laboratoryAssistant->image = '/uploads/laboratoryAssistants/laboratoryAssistantImage' . $laboratoryAssistant->id . '.' . $image->getExtension();
        }
        if($user->save()&&$laboratoryAssistant->save()){
            $transaction->commit();
            return true;
        }
        else{
            $transaction->rollBack();
            return false;
        }
    }

}