<?php

namespace modules\laboratory\models\forms;

use common\models\entity\LaboratoryAssistant;
use common\models\entity\PreRecordsProcedures;
use common\models\entity\PreRecordsProceduresAttachments;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\Exception;

class EditPreRecordProcedureForm extends Model
{
    public $patient;
    public $patientFullName;
    public $doctor;
    public $doctorFullName;
    public $visit_date;
    public $procedure_type_id;
    public $status;
    public $description;
    public $record_id;
    public $attachments;
    public $laboratory_assistant_id;
    public $patient_card_history_id;
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'patientFullName' => 'Пацієнт',
            'doctorFullName' => 'Лікар',
            'visit_date' => 'Дата',
            'procedure_type_id' => 'Тип процедури',
            'patient_card_history_id' => 'Запис карти пацієнта',
            'status' => 'Статус',
            'description'=> 'Примітки',
            'attachments'=> 'Вкладення',
        ];
    }

    /**
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function __construct($record_id, $config = [])
    {
        parent::__construct($config);
        $record=PreRecordsProcedures::findOne($record_id);
        if($record){
            $laboratory_assistant_id=LaboratoryAssistant::find()->where(['user_id'=>Yii::$app->user->getId()])->one();
            if($laboratory_assistant_id){
                $this->laboratory_assistant_id=$laboratory_assistant_id->id;
                $this->record_id=$record_id;
                $this->patientFullName=$record->patient->getFullPatientName();
                $this->doctorFullName=$record->doctor->getFullDoctorName();
                $this->visit_date= Yii::$app->formatter->asDate($record->visit_date);
                $this->status=$record->status;
                $this->doctor=$record->doctor;
                $this->patient=$record->patient;
                $this->description=$record->description;
                $this->patient_card_history_id=$record->patient_card_history_id;
                $this->procedure_type_id=$record->procedureType->name;
                $this->attachments=PreRecordsProceduresAttachments::find()->where(['pre_record_procedure_id'=>$record_id])->all();
            }
            else{
                throw new Exception('Такого лаборанта не існує');
            }
        }
        else{
            throw new Exception('Такого запису не існує');
        }
    }

    public function rules(): array
    {
        return [
            [['status'], 'integer'],
            [['description'],'string'],
        ];
    }

    public function saveProcedure(){
        $model=PreRecordsProcedures::findOne($this->record_id);
        $model->status=$this->status;
        $model->description=$this->description;
        if($model->save()){
            return true;
        }
        return false;
    }

}