<?php

namespace modules\patient\controllers;

use common\models\classes\FormattingSchedule;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class HospitalScheduleController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]

        );
    }
    public function actionIndex()
    {
        $data=new FormattingSchedule();
        return $this->render('index', [
            'data' => $data,
        ]);
    }
}
