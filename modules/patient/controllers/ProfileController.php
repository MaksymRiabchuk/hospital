<?php

namespace modules\patient\controllers;

use common\models\classes\FormattingSchedule;
use common\models\entity\HospitalSchedule;
use common\models\forms\ChangePasswordForm;
use common\models\search\HospitalScheduleSearch;
use common\models\User;
use DateTime;
use modules\patient\models\forms\ChangePatientProfileForm;
use Symfony\Component\String\Inflector\FrenchInflector;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class ProfileController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            'roles' => ['patient-profile'],
                        ],
                        [
                            'actions' => ['change-password'],
                            'allow' => true,
                            'roles' => ['change-patient-password'],
                        ],
                    ],
                ],
            ]
        );
    }

    public function actionIndex()
    {
        $model=new ChangePatientProfileForm();
        if($model->load($this->request->post())){
            $model->save();
            return $this->redirect('index');
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionChangePassword()
    {
        $model=new ChangePasswordForm();
        if($model->load($this->request->post()) &&  $model->validate()){
            $model->changePassword();
            Yii::$app->session->addFlash('success', Yii::t('app','Успішно змінено'));
            return $this->redirect('index');
        }
        return $this->render('change-password', [
            'model' => $model,
        ]);
    }
}
