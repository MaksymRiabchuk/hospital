<?php

namespace modules\patient\controllers;

use common\models\classes\FormattingSchedule;
use common\models\entity\PatientCardHistory;
use modules\patient\models\classes\FindRecordWithModels;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class PatientCardHistoryController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => ['index'],
                            'allow' => true,
                            'roles' => ['view-patient-card-history'],
                        ],
                    ],
                ],
            ]

        );
    }
    public function actionIndex(): string
    {
        $class= new FindRecordWithModels();
        $records=$class->findRecords();
        return $this->render('index',[
            'records'=>$records,
        ]);
    }
}
