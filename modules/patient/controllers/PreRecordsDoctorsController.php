<?php

namespace modules\patient\controllers;

use common\models\classes\FormattingRecord;
use common\models\classes\FormattingSchedule;
use common\models\entity\Doctor;
use common\models\entity\HospitalSchedule;
use common\models\entity\PreRecordsDoctors;
use common\models\entity\ScheduleRecordsDoctors;
use common\models\search\HospitalScheduleSearch;
use DateTime;
use Symfony\Component\String\Inflector\FrenchInflector;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class PreRecordsDoctorsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => ['index','change-doctor','confirm-record','success'],
                            'allow' => true,
                            'roles' => ['create-record'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all HospitalSchedule models.
     *
     * @return string
     * @throws \Exception
     */
    public function actionIndex($dayTimeStamp)
    {
        $doctor = Doctor::find()->one();
        $schedule = new FormattingRecord($dayTimeStamp, $doctor->id);
        return  $this->render('index', [
            'schedule' => $schedule,
            'doctor_id' => $doctor->id,
            'dayTimeStamp' => $dayTimeStamp,
        ]);
    }

    public function actionChangeDoctor()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $day = Yii::$app->request->post('day');
        $doctor_id = Yii::$app->request->post('doctor_id');
        $schedule = new FormattingRecord($day, $doctor_id);
        return $this->renderPartial('_time-table', [
            'day' => Yii::$app->request->post('day'),
            'doctor_id' => Yii::$app->request->post('doctor_id'),
            'schedule' => $schedule->viewTimeSchedule(),
        ]);
    }

    public function actionConfirmRecord()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new PreRecordsDoctors();
        $model->visit_date = (Yii::$app->request->post('time'));
        $model->doctor_id = intval(Yii::$app->request->post('doctor_id'));
        $model->user_id = Yii::$app->user->getId();
        if ($model->save()) {
            return ['result'=>'ok','model_id'=>$model->id];
        } else {
            return ['result'=>'error','errors'=>$model->errors,'model'=>$model];
        }
    }

    public function actionSuccess($id)
    {
        $model=PreRecordsDoctors::findOne($id);

        return $this->render('success',['model'=>$model]);
    }

}
