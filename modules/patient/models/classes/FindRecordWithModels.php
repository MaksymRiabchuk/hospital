<?php

namespace modules\patient\models\classes;


use common\models\entity\Doctor;
use common\models\entity\LaboratoryAssistant;
use common\models\entity\Patient;
use common\models\entity\PatientCardHistory;
use common\models\enums\ObjectTypesInPatientCard;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\web\UploadedFile;

class FindRecordWithModels extends Model
{
    private $array;
    public function findRecords(){
        $records=PatientCardHistory::find()->where(['patient_id'=>\Yii::$app->user->identity])->all();
        foreach ($records as $record){
            /* @var $record PatientCardHistory */
            if($record->object_type==ObjectTypesInPatientCard::DOCTOR){
                $object=Doctor::findOne($record->object_id);
            }
            else{
                $object=LaboratoryAssistant::findOne($record->object_id);
            }
            $this->array[]=[
                'visit_date'=>$record->visit_date,
                'object'=>$object,
                'description'=>$record->description,
            ];
        }
        return $this->array;
    }

}