<?php

use common\models\classes\FormattingRecord;
use common\models\classes\FormattingSchedule;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::t('app','Графік лікарні');
$this->params['breadcrumbs'][] = $this->title;
$schedule = new FormattingSchedule();
?>

<div class="hospital-schedule-index">

    <table class="table table-bordered">
        <thead>
        <tr>
            <td>
                Понеділок
            </td>
            <td>
                Вівторок
            </td>
            <td>
                Середа
            </td>
            <td>
                Четвер
            </td>
            <td>
                П'ятниця
            </td>
            <td>
                Субота
            </td>
            <td>
                Неділя
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <?php $k = 0; ?>
            <?php foreach ($schedule->viewSchedule() as $key => $day): ?>
            <?php $k++ ?>
            <?php if ($day): ?>
                <?php isset($day['url']) ? $url = $day['url'] : $url = '#'; ?>
                <?php isset($day['color']) ? $color = $day['color'] : $color = FormattingRecord::LOCK_COLOR ?>
                <?php if ($color == FormattingRecord::LOCK_COLOR): ?>
                <?= '<td style="background-color:' . $color . ' ">' ?>
                <?php if ($day['dayTimeStamp'] === false): ?>
                    <?= date('d.m.Y', $key) ?>
                <?php else: ?>
                    <?=date('d.m.Y',$key);?>
                <?php endif; ?>
                <?php else:?>
                    <?= '<td style="background-color:' . $color . ' ">' ?>
                    <?php if ($day['dayTimeStamp'] === false): ?>
                        <?= date('d.m.Y', $key) ?>
                    <?php else: ?>
                        <?= Html::a(date('d.m.Y', $key), Url::to([
                            '/patient' . $url,

                            'dayTimeStamp' => $day['dayTimeStamp'],
                            'user_id' => Yii::$app->user->identity->getId(),
                        ]),

                            ['style' => 'color:black;font-weight:bold']
                        ) ?>
                    <?php endif; ?>
                <?php endif;?>
                </td>
            <?php else: echo 'Дня не інє' ?>
            <?php endif; ?>
            <?php if ($k % 7 == 0): ?>
        </tr>
        <?php endif ?>
        <?php endforeach; ?>
        </tr>
        </tbody>
    </table>


</div>
