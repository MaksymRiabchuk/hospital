<?php use common\models\entity\PatientCardHistory;use common\models\enums\ObjectTypesInPatientCard;?>
<?php if ($records):?>
<table class="table">
    <thead>
        <tr>
            <th>
                Дата візиту
            </th>
            <th>
                Мед працівник
            </th>
            <th>
                Примітки
            </th>
        </tr>
    </thead>
    <tbody>

        <?php foreach ($records as $record):?>
            <tr>
                <td>
                    <?=Yii::$app->formatter->asDate($record['visit_date'])?>
                </td>
                <td>
                        <?=$record['object']->getFullName()?>
                </td>
                <td>
                    <?=$record['description']?>
                </td>
            </tr>
        <?php endforeach;?>

    </tbody>
</table>
<?php else:?>
У вас немає жодного запису в карті пацієнта
<?php endif;?>