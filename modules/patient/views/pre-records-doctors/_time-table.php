<?php

use common\models\classes\FormattingRecord;

/* @var $this yii\web\View */
/* @var $schedule FormattingRecord */
$this->title = 'Запис до лікаря';
$this->params['breadcrumbs'][] = $this->title;
?>

<table class="table table-bordered">
    <tbody>
    <tr>
        <?php $k = 0; ?>
        <?php foreach ($schedule as $key => $time): ?>
        <?php $k++ ?>
        <?php isset($time['color']) ? $color = $time['color'] : $color = FormattingRecord::EMPTY_COLOR ?>
        <?= '<td style="background-color:' . $color . ' ">' ?>
        <a data-dateTimeStamp="<?= $key ?>" class="record-time" href="#" style="color: #0a0a0a">
            <?= date('H:i', $key); ?>
        </a>
        <?php if ($key): ?>

        <?php else: echo 'Дня не іcнує' ?>
        <?php endif; ?>
        <?php if ($k % 12 == 0): ?>
    </tr>
    <?php endif ?>
    <?php endforeach; ?>
    </tbody>
</table>


