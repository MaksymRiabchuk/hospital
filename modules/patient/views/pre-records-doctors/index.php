<?php

use common\models\classes\FormattingRecord;
use common\models\entity\Doctor;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $schedule FormattingRecord */
/* @var $dayTimeStamp int */
$this->title = 'Графік лікарні';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Графік лікарні'), 'url' => Url::to('/patient/hospital-schedule/index')];

?>

    <div class="hospital-schedule-index">
        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-6 choose-doctor">
                <?= $form->field($schedule, 'doctor')->dropDownList(Doctor::getDoctorsList()) ?>
            </div>
            <input id="dayTimeStampInput" type="hidden" value="<?=$dayTimeStamp?>">
        </div>

        <?php ActiveForm::end() ?>
        <div class="row">
            <div id="select-time-table">
                <?= $this->render('_time-table',['schedule'=>$schedule->viewTimeSchedule()]) ?>
            </div>
        </div>
    </div>
<?php
$script = <<<JS
let selectedDoctor=$("#formattingrecord-doctor").val();
let selectedTime;
$(document).on('change',"#formattingrecord-doctor",function (){
    selectedDoctor=Number($(this).val())
    selectedTime=$("#dayTimeStampInput").val();
    $.ajax({
        url:"/patient/pre-records-doctors/change-doctor",
        type:'post',
        dataType:'JSON',
        data:{doctor_id:selectedDoctor,day:selectedTime},
        success:function (data){
            $('#select-time-table').html(data);
        }
    })
})

$(document).on('click',".record-time",function (){
    if(!confirm('Ви дійсно хочете записатися?')){
        
    }
    else {
        if(selectedDoctor==null){
        alert('Виберіть лікаря');
    }
    else{
        let color=$(this).css('background-color');
            if(color=='rgb(238, 171, 153)'){
                alert('Цей час вже зайнятий')
            }
            else{
                selectedTime=$(this).attr('data-datetimeStamp');
                $.ajax({
                    url: "/patient/pre-records-doctors/confirm-record",
                    type: 'post',
                    dataType: 'JSON',
                    data:{doctor_id:selectedDoctor, time:selectedTime},
                    success: function(response) {
                        if(response.result=='ok'){
                            
                            window.location.href="success?id="+response.model_id    
                        }
                        else{
                            window.location.href="error"
                        }
                    }
                });
        }
    }
    }
    
    
})
JS;
$this->registerJs($script);
