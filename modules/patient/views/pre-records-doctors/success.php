<?php

use common\models\entity\Doctor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entity\PreRecordsDoctors */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app','Створений запис');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="schedule-form">
    <div class="alert alert-success">
        <strong style="color: #0a0a0a">
            <?= Yii::t('app', 'Ваш лікар:') ?>
        </strong>
        <i>
            <?= Doctor::getDoctorNameById($model->doctor_id) ?>
        </i>
        <br>
        <strong style="color: #0a0a0a">
            <?= Yii::t('app', 'Вибраний час:') ?>
        </strong>
        <i>
            <?= date('Y.m.d H:i',$model->visit_date) ?>
        </i>
    </div>

</div>
