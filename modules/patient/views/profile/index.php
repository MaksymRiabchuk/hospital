<?php
/* @var $model ChangePatientProfileForm /*/

use common\models\User;
use kartik\file\FileInput;
use modules\patient\models\forms\ChangePatientProfileForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>
<h3 style="text-align: center">Профіль пацієнта</h3>

<?php $form= ActiveForm::begin()?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'imageFile')->widget(FileInput::classname(),
            [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'initialPreview' => (!$model->image) ? false : $model->image,
                    'initialPreviewAsData' => true,
                    'showPreview' => true,
                    'showRemove' => false,
                    'showUpload' => false
                ],
            ]); ?>
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <?=$form->field($model,'username')->textInput(['readOnly'=>true]);?>
            </div>
            <div class="col-md-6">
                <?=$form->field($model,'phone')->widget(MaskedInput::class,[
                    'mask' => '38(099) 999-9999',
                ]);?>
            </div>
            <div class="col-md-6">
                <?=$form->field($model,'email')->textInput(['readOnly'=>true]);?>
            </div>
            <div class="col-md-6">
                <?=$form->field($model,'name')->textInput();?>
            </div>
            <div class="col-md-6">
                <?=$form->field($model,'surname')->textInput();?>
            </div>
            <div class="col-md-6">
                <?=$form->field($model,'parent_name')->textInput();?>
            </div>
            <div class="col-md-6">
                <br>
                <?= Html::a(Yii::t('app','Змінити пароль'),['profile/change-password'],['class'=>'btn btn-primary']) ?>
            </div>
            <div class="col-md-6">
                <br>
                <?= Html::submitButton(Yii::t('app','Зберегти'), ['class' => 'btn btn-success','style'=>'float:right']) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end()?>
