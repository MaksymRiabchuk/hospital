<?php

namespace modules\doctorprofile\controllers;

use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\entity\PatientCardHistory;
use common\models\entity\PreRecordsDoctors;
use common\models\entity\PreRecordsProcedures;
use common\models\entity\PreRecordsProceduresAttachments;
use common\models\entity\ProcedureTypes;
use common\models\enums\ObjectTypesInPatientCard;
use common\models\enums\PreRecordsStatusTypes;
use modules\doctorprofile\models\classes\DoctorRecords;
use modules\doctorprofile\models\forms\AddProcedureToPatientRecord;
use modules\doctorprofile\models\forms\AddRecordToPatientCardHistory;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class RecordsDoctorsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => ['index', 'add-record-to-patient-history', 'add-procedure', 'delete-procedure',
                                'change-status-record','view-procedure'],
                            'allow' => true,
                            'roles' => ['doctor-records-index'],
                        ],
                    ],
                ],
            ]
        );
    }

    private $array = [];

    /**
     * Lists all HospitalSchedule models.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        //Якщо запис менший за поточну дату його не добавляти

        $model = new DoctorRecords();
        $records = $model->viewRecords();
        return $this->render('index', [
            'records' => $records,
        ]);
    }

    public function actionChangeStatusRecord($record_id)
    {
        $record = PreRecordsDoctors::find()->where(['id' => $record_id])->one();
        /* @var $record PreRecordsDoctors */
        if ($record) {
            $record->status = PreRecordsStatusTypes::DIDNT_COME;
            $record->save();
            return $this->redirect('index');
        } else {
            throw new Exception('Такого запису не існує');
        }

    }

    /**
     * @throws Exception
     */
    public function actionAddRecordToPatientHistory($record_id)
    {
        $record=PreRecordsDoctors::findOne($record_id);
        if(!$record){
            throw new Exception('Такого запису не існує');
        }
        $record->status=PreRecordsStatusTypes::COME;
        $record->save();
        $model = new AddRecordToPatientCardHistory($record_id);
        $model->getRecordPatientCardHistory();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->saveRecord()) {
                Yii::$app->session->setFlash('success', "Успішно добавлено запис");
                return $this->redirect('index');

            } else {
                throw new Exception('Помилка при збережені запису в карту пацієнта');
            }
        }

        return $this->render('add-record-to-patient-history', [
            'model' => $model,
        ]);

    }

    public function actionAddProcedure()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $procedure_type_id = Yii::$app->request->post('procedure_type_id');
        $visit_date = Yii::$app->request->post('visit_date');
        $patient_card_history_id = Yii::$app->request->post('patient_card_history_id');
        $patient_id = Yii::$app->request->post('patient_id');
        $result = $this->addProcedure($procedure_type_id, $visit_date, $patient_card_history_id, $patient_id);
        $procedureList = PreRecordsProcedures::find()->where(['patient_card_history_id' => $patient_card_history_id])->all();
        return $this->renderPartial('_procedure-list', ['procedureList' => $procedureList]);
    }

    private function addProcedure($procedure_type_id, $visit_date, $patient_card_history_id, $patient_id)
    {
        $model = new PreRecordsProcedures();
        $user = Yii::$app->user->identity;
        $doctor = Doctor::find()->where(['user_id' => $user->id])->one();
        /* @var $doctor Doctor */
        if ($doctor) {
            $model->patient_id = $patient_id;
            $model->doctor_id = $doctor->id;
            $model->visit_date = strtotime($visit_date);
            $model->procedure_type_id = $procedure_type_id;
            $model->status = PreRecordsStatusTypes::IN_WAIT;
            $model->patient_card_history_id = $patient_card_history_id;
            $model->save();
            if ($model->errors) {
                return $model->errors;
            }
            return true;
        } else {
            return false;
        }

    }

    public function actionDeleteProcedure()
    {
        Yii::$app->response->format=Response::FORMAT_JSON;
        $record_id = Yii::$app->request->post('record_id');
        $patient_card_history_id= Yii::$app->request->post('patient_card_history_id');
        $record = PreRecordsProcedures::findOne($record_id);
        if ($record) {
            $record->delete();

        }
        $procedureList = PreRecordsProcedures::find()->where(['patient_card_history_id' => $patient_card_history_id])->all();
        return $this->renderPartial('_procedure-list', ['procedureList' => $procedureList]);
    }

    /**
     * @throws Exception
     */
    public function actionViewProcedure($procedure_id, $record_id){
        $doctorRecord=PreRecordsDoctors::findOne($record_id);
//        $patientCardHistory=PatientCardHistory::findOne()
        $procedureRecord=PreRecordsProcedures::findOne($procedure_id);
        if(!$doctorRecord or !$procedureRecord){
            throw new Exception('Такого запису не існує');
        }
        $arrayAttachments=PreRecordsProceduresAttachments::find()->where(['pre_record_procedure_id'=>$procedure_id])->all();
        return $this->render('view-procedure',[
           'attachments'=>$arrayAttachments,
           'procedureRecord'=>$procedureRecord,
            'record_id'=>$record_id,
        ]);
    }
}
