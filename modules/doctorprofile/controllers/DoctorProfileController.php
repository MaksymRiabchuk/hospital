<?php /** @noinspection PhpUnused */

namespace modules\doctorprofile\controllers;

use common\models\forms\ChangePasswordForm;
use modules\doctorprofile\models\forms\ChangeDoctorProfileForm;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class DoctorProfileController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'actions' => ['change-doctor','index'],
                            'allow' => true,
                            'roles' => ['doctor-profile'],
                        ],
                        [
                            'actions' => ['change-password'],
                            'allow' => true,
                            'roles' => ['change-doctor-password'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function actionIndex()
    {
        $model = new ChangeDoctorProfileForm();
        if ($model->load($this->request->post())) {
            $model->save();
            Yii::$app->session->addFlash('success', Yii::t('app','Успішно змінено'));
            return $this->redirect('index');
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionChangePassword()
    {
        $model = new ChangePasswordForm();
        if ($model->load($this->request->post()) && $model->validate()) {
            $model->changePassword();
            Yii::$app->session->addFlash('success', Yii::t('app', 'Успішно змінено'));
            return $this->redirect('index');
        }
        return $this->render('change-password', [
            'model' => $model,
        ]);
    }
}
