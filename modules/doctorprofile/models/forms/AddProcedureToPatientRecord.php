<?php

namespace modules\doctorprofile\models\forms;


use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\web\UploadedFile;

class AddProcedureToPatientRecord extends Model
{
    public $procedure;

    public function rules()
    {
        return [
            [['procedure'], 'required'],
            ['procedure','integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'procedure' => Yii::t('app', 'Процедури'),
        ];
    }


}