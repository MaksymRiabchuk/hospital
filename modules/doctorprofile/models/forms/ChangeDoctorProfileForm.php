<?php

namespace modules\doctorprofile\models\forms;


use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\web\UploadedFile;

class ChangeDoctorProfileForm extends Model
{
    public $surname;
    public $name;
    public $parent_name;
    public $image;
    public $specialization_id;
    public $imageFile;

    public $username;
    public $phone;
    public $email;

    public function rules()
    {
        return [
            [['surname', 'name', 'parent_name', 'phone','specialization_id'], 'required'],
            [['imageFile'], 'string'],
        ];
    }

    public function __construct($config = [])
    {
        parent::__construct($config);

        if (Yii::$app->user->isGuest) {
            throw new Exception('Змінювати профіль можуть лише авторизовані лікарям');
        }

        $user = Yii::$app->user->identity;
        /* @var $user User */
        $this->username = $user->username;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->surname = $user->doctor->surname;
        $this->name = $user->doctor->name;
        $this->parent_name = $user->doctor->parent_name;
        $this->image = $user->doctor->image;
        $this->specialization_id = $user->doctor->specialization_id;
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Нікнейм'),
            'phone' => Yii::t('app', 'Телефон'),
            'email' => Yii::t('app', 'Ел. пошта'),
            'name' => Yii::t('app', 'Ім`я'),
            'surname' => Yii::t('app', 'Прізвище'),
            'parent_name' => Yii::t('app', 'По батькові'),
            'imageFile' => Yii::t('app', 'Фото'),
            'specialization_id' => Yii::t('app', 'Спеціалізація'),
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            throw new Exception('Помилка збереження');
        }
        $transaction=Yii::$app->db->beginTransaction();
        $user = Yii::$app->user->identity;
        $user->phone = $this->phone;
        $doctor=Doctor::find()->where(['user_id'=>$user->id])->one();
        /* @var $doctor Doctor */
        $doctor->name=$this->name;
        $doctor->surname=$this->surname;
        $doctor->parent_name=$this->parent_name;
        $doctor->specialization_id=$this->specialization_id;
        if (!$this->image) {
            $this->image = '';
        }
        $image = UploadedFile::getInstance($this, 'imageFile');
        if ($image) {
            $res = $image->saveAs('uploads/doctors/doctorImage' . $doctor->id . '.' . $image->getExtension());
            $doctor->image = '/uploads/doctors/doctorImage' . $doctor->id . '.' . $image->getExtension();
        }
        if($user->save()&&$doctor->save()){
            $transaction->commit();
            return true;
        }
        else{
            $transaction->rollBack();
            return false;
        }
    }

}