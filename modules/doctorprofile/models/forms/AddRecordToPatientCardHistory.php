<?php

namespace modules\doctorprofile\models\forms;


use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\entity\PatientCardHistory;
use common\models\entity\PreRecordsDoctors;
use common\models\entity\PreRecordsProcedures;
use common\models\enums\ObjectTypesInPatientCard;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\web\UploadedFile;

class AddRecordToPatientCardHistory extends Model
{
    public $doctor;
    public $patient;
    public $record_id;
    public $id;
    public $patientFullName;
    public $visit_date;
    public $description;
    public $proceduresList;
    public function rules()
    {
        return [
            [['id','doctor','patient','visit_date'], 'required'],
            [['id','doctor','patient','visit_date'],'integer'],
            [['description'],'string'],
        ];
    }

    /**
     * @throws Exception
     */
    public function __construct($record_id,$config = [])
    {
        parent::__construct($config);
        $record = PreRecordsDoctors::find()->where(['id' => $record_id])->one();
        /* @var $record PreRecordsDoctors */
        if(!$record){
            throw new Exception(Yii::t('app','Такого запису не існує'));
        }
        $this->record_id=$record_id;
        $this->doctor=$record->doctor_id;
        $this->visit_date=$record->visit_date;
        $patient=Patient::findPatientByUserId($record->user_id);
        /* @var $patient Patient */
        $this->patient=$patient->id;
        $this->patientFullName=$patient->getFullPatientName();
        $this->proceduresList=PreRecordsProcedures::find()->where(['patient_card_history_id'=>$record->patient_card_history_id])->all();
    }

    public function attributeLabels()
    {
        return [
            'doctor' => Yii::t('app', 'Лікар'),
            'patient' => Yii::t('app', 'Пацієнт'),
            'visit_date' => Yii::t('app', 'Дата прийому'),
            'description' =>Yii::t('app','Примітки'),
            'proceduresList'=>Yii::t('app','Список процедур'),
        ];
    }

    /**
     * @throws Exception
     */
    public function getRecordPatientCardHistory(){
        $record=PreRecordsDoctors::find()->where(['id'=>$this->record_id])->one();
        /* @var $record PreRecordsDoctors */
        if($record){
            if($record->patient_card_history_id==null){
                $model=new PatientCardHistory();
                $model->visit_date=$this->visit_date;
                $model->patient_id=$this->patient;
                $model->object_type=ObjectTypesInPatientCard::DOCTOR;
                $model->object_id=$this->doctor;
                $model->save();
                $this->id=$model->primaryKey;

                $record->patient_card_history_id=$this->id;
                $record->save();
            }
            else{
                $this->id=$record->patient_card_history_id;
                $this->description=$record->patientCardHistory->description;
            }
        }
        else{
            throw new Exception('Такого запису  не існує');
        }

    }
    public function saveRecord(){
        $model=PatientCardHistory::findOne($this->id);
        $model->description=$this->description;
        $model->save();
        if($model->errors){
            return false;
        }
        return true;
    }


}