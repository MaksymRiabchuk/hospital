<?php

namespace modules\doctorprofile\models\classes;

use common\models\entity\Doctor;
use common\models\entity\PreRecordsDoctors;
use common\models\entity\ScheduleRecordsDoctors;
use common\models\enums\PreRecordsStatusTypes;
use common\models\User;
use DateInterval;
use DateTime;
use Yii;
use yii\db\Exception;
use yii\helpers\Url;

class DoctorRecords
{
    private $doctor;
    private $startDay;
    private $endDay;
    const MINUTESINTERVAL=900;
    private $records=[];

    public function __construct()
    {
        $day=strtotime(date('m.d.y'));
        $user_id= Yii::$app->user->getId();
        $user=User::findOne($user_id);
        $doctor=Doctor::find()->where(['user_id'=>$user_id])->one();
        if($user){
            if ($user->role==1){
                $this->doctor=$doctor->id;
            }
            else{
                echo 'Недостатньо прав';
            }
        }
        else{
            echo 'Такого користувача немає';
        }
        $this->findStartTime($day);
        $this->findEndTime();
        $this->findRecords($this->startDay,$this->endDay);
    }
    private function findStartTime($start_day){
        $selectedDay=new DateTime(date('d.m.y',$start_day));
        $selectedDay=$selectedDay->format('Y-m-d 00:00');
        $this->startDay=strtotime($selectedDay);
    }
    private function findEndTime(){
        $selectedDay=new DateTime(date('d.m.y',$this->startDay));
        $interval=DateInterval::createFromDateString('1 day');
        $end_day=$selectedDay->add($interval);
        $this->endDay=$end_day->getTimestamp();
    }
    private function findRecords($startTime,$endTime){
        $models=PreRecordsDoctors::find()
            ->where(['doctor_id'=>$this->doctor])
            ->andWhere(['between','visit_date',$startTime,(time()-self::MINUTESINTERVAL)])->orderBy(['visit_date'=>SORT_DESC])->all();
        foreach ($models as $model){
            /* @var $model PreRecordsDoctors */
            $model->status=PreRecordsStatusTypes::DIDNT_COME;
            $model->save();
        }
        $records=PreRecordsDoctors::find()
            ->where(['doctor_id'=>$this->doctor])
            ->andWhere(['between','visit_date',$startTime,$endTime])->orderBy(['visit_date'=>SORT_DESC])->asArray()
            ->all();
        $this->records=$records;
    }
    public function viewRecords(){
        foreach ($this->records as $key=>$record) {
            if($record['status']==PreRecordsStatusTypes::DIDNT_COME){
                $this->records[$key]['color']=Yii::$app->params['lockColor'];
            }
            else{
                if($record['status']!=PreRecordsStatusTypes::COME){
                    $this->records[$key]['color']=$this->setColorToRecord($record['visit_date']);
                }
                else{
                    $this->records[$key]['color']=Yii::$app->params['emptyColor'];
                }
            }
        }
        return $this->records;
    }
    private function setColorToRecord($visit_date){

        $currentTime=time()-self::MINUTESINTERVAL;
        if($currentTime-$visit_date>86400){
            return Yii::$app->params['doctorRecordDeny'];
        }
        if ($currentTime-$visit_date>0){
            return Yii::$app->params['lockColor'];
        }
        return '#9999999';
    }

}