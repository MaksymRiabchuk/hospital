<?php

namespace modules\doctorprofile\models\classes;

use common\models\entity\ScheduleRecordsDoctors;
use common\models\User;
use DateInterval;
use DateTime;
use yii\base\Model;
use yii\db\Exception;
use yii\helpers\Url;
use yii\web\UploadedFile;

class EditProfileDoctor extends Model
{
    public $phone;
    private $password;
    private $confirmPassword;

    public $imageFile;
    public $image;

    public function __construct()
    {
        $this->phone=\Yii::$app->request->post(['phone']);
        $this->password=\Yii::$app->request->post(['password']);
        $this->confirmPassword=\Yii::$app->request->post(['confirmPassword']);
        if ($this->password!==$this->confirmPassword){
            throw new Exception(\Yii::t('app','Паролі не співпадають'));
        }
        if(\Yii::$app->request->post(['imageFile'])){
            $this->imageFile=\Yii::$app->request->post(['imageFile']);
            $this->downloadPhoto();
        }
    }
    public function downloadPhoto()
    {
        $image = UploadedFile::getInstance($this, 'imageFile');
        if ($image) {
            $filename='uploads/doctors'.mktime().$image->getExtension();
            $image->saveAs($filename);
            return '/'.$filename;
        }
        if ($this->image){
            return $this->image;
        }
        return null;
    }

}