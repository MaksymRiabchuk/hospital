<?php
/* @var $records array */

use common\models\entity\Patient;
use common\models\enums\PreRecordsStatusTypes;
use yii\helpers\Html;

?>
<table class="table table-bordered" style="max-width: 1500px">
    <thead>
    <tr>
        <td style="width: 200px">
            Пацієнт
        </td>
        <td style="width: 200px">
            Час прийому
        </td>
        <td style="width: 1100px">
            Статус
        </td>
        <td>

        </td>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($records as $record): ?>
        <tr style="background-color:<?= $record['color'] ?> ">
            <?php $user = Patient::findPatientByUserId($record['user_id']) ?>
            <td>
                <?= isset($user) ? $user->surname . ' ' . $user->name . ' ' . $user->parent_name : 'Невідомий  користувач' ?>
            </td>
            <td>
                <?= date('d.m.y H:i', $record['visit_date']); ?>
            </td>
            <td>
                <?= PreRecordsStatusTypes::getLabel($record['status']);  ?>
            </td>
            <td>
                <?php if ($record['color']!=Yii::$app->params['lockColor'] &&$record['color']!=Yii::$app->params['emptyColor']):?>
                <?= Html::a('Прийшов', [
                    '/doctorprofile/records-doctors/add-record-to-patient-history',
                    'record_id' => $record['id'],
                ],['class' => 'btn btn-success']) ?>
                <?= Html::a('Не прийшов', [
                    '/doctorprofile/records-doctors/change-status-record',
                    'record_id' => $record['id'],
                ],['class' => 'btn btn-danger']) ?>
                <?php elseif ($record['status']==PreRecordsStatusTypes::COME):?>
                    <?= Html::a('Прийшов', [
                        '/doctorprofile/records-doctors/add-record-to-patient-history',
                        'record_id' => $record['id'],
                    ],['class' => 'btn btn-success']) ?>
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>

</table>
