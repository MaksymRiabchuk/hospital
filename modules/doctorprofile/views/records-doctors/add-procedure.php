<?php use common\models\entity\ProcedureTypes;
use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>
<?php $form = ActiveForm::begin() ?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'procedure')->dropDownList(ProcedureTypes::getProcedureTypesById()) ?>
    </div>
    <div class="col-md-4" style="margin-top: 30px">
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>
