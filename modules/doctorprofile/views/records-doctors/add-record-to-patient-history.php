<?php

use common\models\entity\ProcedureTypes;
use modules\doctorprofile\models\forms\AddRecordToPatientCardHistory;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $model AddRecordToPatientCardHistory */
/* @var $this View */
?>
    <div class="row">
        <div class="col-md-6">
            <div>
                <label><strong>Пацієнт:</strong></label>
            </div>

            <?= $model->patientFullName ?>
        </div>
        <div class="col-md-6">
            <div>
                <label><strong>Дата візиту:</strong></label>
            </div>
            <?= Yii::$app->formatter->asDate($model->visit_date); ?>
        </div>
    </div>
<?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'patient')->hiddenInput()->label(false) ?>
            <div><strong>Примітки:</strong></div>
            <?= $form->field($model, 'description')->textarea(['rows' => 5])->label(false) ?>
        </div>
        <div class="col-md-4">
            <div style="margin-bottom: 10px">
                <?= Html::a('Добавити процедуру', '#',
                    ['class' => 'btn btn-primary', 'style' => 'margin-top:47px', 'id' => 'add-procedure-btn']) ?>
            </div>
            <div>
                <strong>Список процедур</strong>
            </div>
            <div id="div-procedure-list">
                <?=$this->render('_procedure-list',['procedureList'=>$model->proceduresList,'record_id'=>$model->record_id]) ?>
            </div>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end() ?>
<?php Modal::begin([
    "id" => "procedureList",
    "title" => 'Добавити процедуру',
    "footer" => '<a class="btn btn-danger" id="btn-cancel-procedureTypesList">Відмінити</a>
 <a class="btn btn-success" id="btn-select-procedureType">Зберегти</a>',
]) ?>
<?php $form = ActiveForm::begin() ?>
<?= $form->field($model, 'proceduresList')->dropDownList(ProcedureTypes::getProcedureTypesById()) ?>
    <label for="start">Дата запису</label>

    <input type="date" id="procedure-visit-date" name="trip-start"
           value="<?= date('Y-m-d') ?>"
           class="form-control">
<?php ActiveForm::end() ?>
<?php Modal::end(); ?>
<?php $script = <<<JS
let selectedProcedureType;
let procedureVisitDate;
let patientCardHistory=$('#addrecordtopatientcardhistory-id').val();
$("#add-procedure-btn").on('click',function() {
    $("#procedureList").modal();
});

$("#btn-cancel-procedureTypesList").on('click',function() {
    $("#procedureList").modal('hide');
});

$("#btn-select-procedureType").on('click',function (){
    selectedProcedureType=$('#addrecordtopatientcardhistory-procedureslist').val();
    procedureVisitDate=$('#procedure-visit-date').val();
    $("#procedureList").modal('hide');
    addProcedure();
})

function addProcedure(){
    let patient=$('#addrecordtopatientcardhistory-patient').val();
    
    $.ajax({
          url:"/doctorprofile/records-doctors/add-procedure",
          type:'POST',
          dataType:'JSON',
          data:{patient_card_history_id:patientCardHistory,patient_id:patient,
          procedure_type_id:selectedProcedureType,visit_date:procedureVisitDate},
          success:function (data){
              $('#div-procedure-list').html(data);
          },
          errors:function (data){
              console.log(data);
          }  
    });
}

$(document).on('click',".btn-procedure-delete",function (){
    let preRecordProcedureId=$(this).attr("data-procedure-record-id");
    deleteProcedure(preRecordProcedureId); 
});
function deleteProcedure(record_id){
    $.ajax({
          url:"/doctorprofile/records-doctors/delete-procedure",
          type:'POST',
          dataType:'JSON',
          data:{record_id:record_id,patient_card_history_id:patientCardHistory},
          success:function (data){
              $('#div-procedure-list').html(data);
          },
          errors:function (data){
              console.log(data);
          }  
    });
}
JS;
$this->registerJs($script);
