<?php

use common\models\entity\PreRecordsProcedures;
use common\models\entity\PreRecordsProceduresAttachments;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $attachments array */
/* @var $procedureRecord PreRecordsProcedures */
/* @var $record_id int */
?>
<div class="card card-default">
    <div class="card card-body">
        <div class="row" >
            <div class="col-md-6">
                <?php foreach ($attachments as $attachment): ?>
                <?php /* @var $attachment PreRecordsProceduresAttachments */?>
                    <div>
                        <?= Html::img($attachment->file, ['style' => 'max-width:100px;margin-bottom:10px','class'=>'procedure-img'])?>
                        <?= $attachment->name ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col-md-6">
                <?= $procedureRecord->description; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a('Повернутись', ['records-doctors/add-record-to-patient-history', 'record_id' => $record_id], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "view-procedure-img",
    "title" => 'Перегляд результату дослідження',
    "size"=>'modal-lg',
]) ?>
<?=Html::img('',['style'=>'width:100%','id'=>'preview-img-procedure']) ?>
<?php Modal::end(); ?>
<?php
$script=<<<JS
$('.procedure-img').on('click',function(){
let img=$(this).attr('src');
$('#preview-img-procedure').attr('src',img);
$('#view-procedure-img').modal();

});  
JS;
$this->registerJs($script);

?>
