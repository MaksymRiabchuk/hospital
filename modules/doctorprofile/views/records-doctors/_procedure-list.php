<?php

use common\models\enums\ProcedureVisitStatus;
use yii\bootstrap\Html;?>
<table class="table">
    <thead>
    <tr>
        <th width="80%">
            Аналіз
        </th>
        <th width=" 20%">
            Дії
        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($procedureList as $procedure): ?>
        <tr>
            <td>
                <?= $procedure->procedureType->name ?>
            </td>
            <td>
                <?php if ($procedure->status!= ProcedureVisitStatus::PROCESSED):?>
                <span style="float:right; display: block;" class="btn btn-danger btn-sm btn-procedure-delete"
                      data-procedure-record-id="<?=$procedure->id ?>">X</span>
                <?php else:?>
                    <?= Html::a('Переглянути',['records-doctors/view-procedure','procedure_id'=>$procedure->id,'record_id'=>$record_id]
                        ,['class'=>'btn btn-sm btn-success'])?>
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
