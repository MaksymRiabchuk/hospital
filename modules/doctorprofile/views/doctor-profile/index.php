<?php

use common\models\entity\Specialization;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

?>
<h3 style="text-align: center">Профіль лікаря</h3>
<div class="doctor-form">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'imageFile')->widget(FileInput::classname(),
                    [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'initialPreview' => (!$model->image) ? false : $model->image,
                            'initialPreviewAsData' => true,
                            'showPreview' => true,
                            'showRemove' => false,
                            'showUpload' => false
                        ],
                    ]); ?>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'username')->textInput(['maxlength' => true,'readonly'=>true]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true,]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'surname')->textInput(['maxlength' => true,]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'parent_name')->textInput(['maxlength' => true,]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'phone')->widget(MaskedInput::class,[
                            'name' => 'input-1',
                            'mask' => '(999) 999-9999'
                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true,]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Спеціалізація</label>
                        <p>
                            <?php $specialization=Specialization::findSpecializationById($model->specialization_id)?>
                            <?= isset($specialization)?$specialization->name:'Помилка спеціалізації'?>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success','value'=>'confirmDoctorProfile']) ?>
                    </div>
                    <div  class="col-md-4">
                        <?= Html::a(Yii::t('app','Змінити'),['doctor-profile/change-password'], ['class' => 'btn btn-primary','style'=>'float:right']) ?>
                    </div>
                </div>
            </div>


        </div>
    <?php ActiveForm::end(); ?>

</div>
