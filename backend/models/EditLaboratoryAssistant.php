<?php /** @noinspection DuplicatedCode */
/** @noinspection PhpUnusedParameterInspection */

/** @noinspection PhpUnused */

namespace backend\models;

use common\models\entity\LaboratoryAssistant;
use common\models\enums\RoleTypes;
use common\models\enums\StatusTypes;
use common\models\User;
use Exception;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for table "cabinets".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int|null $is_deleted
 */
class EditLaboratoryAssistant extends Model
{
    public $username;
    public $password;
    public $confirmPassword;
    public $email;
    public $phone;

    public $isNewRecord = true;
    public $user_id;

    public $imageFile;
    public $name;
    public $surname;
    public $parent_name;
    public $image;


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [
                [
                    'username', 'password', 'confirmPassword', 'email',
                    'phone', 'name', 'surname', 'parent_name', 'image',
                ],
                'required',
            ],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Паролі не співпадають'],
            ['email', 'email'],
            ['phone', 'string', 'max' => 14],
            ['email', 'checkEmailUnique'],
            ['username', 'checkUsernameUnique'],
            [['username', 'surname', 'name', 'parent_name'], 'string', 'max' => 32],
            ['password', 'string', 'min' => 4],
        ];
    }

    /**
     * @throws Exception
     */
    public  function __construct($user_id=null, $config=[])
    {
        if(!$user_id==null){
            $this->loadModel($user_id);
        }
        parent::__construct($config);
    }

    /**
     * @throws Exception
     */
    private function loadModel($user_id){
        $this->user_id=$user_id;
        $user=User::findOne($user_id);
        /* @var $assistant LaboratoryAssistant */
        $assistant=LaboratoryAssistant::find()->where(['user_id'=>$user_id])->one();
        if($user && $assistant){
            $this->username=$user->username;
            $this->email=$user->email;
            $this->phone=$user->phone;
            $this->image=$assistant->image;
            $this->name=$assistant->name;
            $this->surname=$assistant->surname;
            $this->parent_name=$assistant->parent_name;
            $this->isNewRecord=false;
        }
        else{
            throw new Exception('Лаборанта не знайдено');
        }
    }
    public function checkEmailUnique($attribute, $params)
    {
        $user = User::find()->where(['email' => $attribute])->one();
        if ($user) {
            $this->addError($attribute, Yii::t('app', 'Такий емайл вже існує'));
        }
    }

    public function checkUsernameUnique($attribute, $params)
    {
        $user = User::find()->where(['username' => $attribute])->one();
        if ($user) {
            $this->addError($attribute, Yii::t('app', 'Такий нікнейм вже існує'));
        }
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'username' =>Yii::t('app','Нікнейм'),
            'email' =>Yii::t('app','Емейл'),
            'phone' =>Yii::t('app','Телефон'),
            'name' =>Yii::t('app','Ім`я'),
            'surname' =>Yii::t('app','Прізвище'),
            'parent_name' =>Yii::t('app','По-батькові'),
            'image' =>Yii::t('app','Фото'),
            'specialization_id' =>Yii::t('app','Спеціалізація'),
        ];
    }

    /**
     * @throws \yii\db\Exception|\yii\base\Exception
     */
    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();
        if ($this->isNewRecord){
            $user = new User();
        }
        else{
            $user=User::findOne($this->user_id);
        }

        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->username = $this->username;
        $user->phone = $this->phone;
        $user->generateAuthKey();
        $user->role = RoleTypes::LABORATORY_ASSISTANT;
        $user->status = StatusTypes::ACTIVE;
        $user->save();
        $user_id = $user->id;
        if($this->isNewRecord){
            $assistant = new LaboratoryAssistant();
        }
        else{
            $assistant=LaboratoryAssistant::find()->where(['user_id'=>$this->user_id])->one();
        }
        $assistant->name = $this->name;
        $assistant->surname = $this->surname;
        $assistant->parent_name = $this->parent_name;
        $assistant->user_id = $user_id;
        $assistant->image =$this->downloadPhoto();
        $assistant->save();
        if ($user->errors || $assistant->errors){
            $transaction->rollBack();
            var_dump($user->errors);
            var_dump($assistant->errors);
            die();
        }
        $transaction->commit();
        return true;
    }

    public function downloadPhoto(): ?string
    {
        $image = UploadedFile::getInstance($this, 'imageFile');
        if ($image) {
            $filename='uploads/doctors'.mktime().$image->getExtension();
            $image->saveAs($filename);
            return '/'.$filename;
        }
        if ($this->image){
            return $this->image;
        }
        return null;
    }
}
