<?php /** @noinspection PhpUnusedParameterInspection */

/** @noinspection PhpUnused */

namespace backend\models;

use common\models\entity\Doctor;
use common\models\enums\RoleTypes;
use common\models\enums\StatusTypes;
use common\models\User;
use Exception;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for table "cabinets".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int|null $is_deleted
 */
class EditDoctor extends Model
{
    public $username;
    public $password;
    public $confirmPassword;
    public $email;
    public $phone;

    public $isNewRecord = true;
    public $user_id;

    public $specialization_id;
    public $imageFile;
    public $name;
    public $surname;
    public $parent_name;
    public $image;


    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [
                [
                    'username', 'password', 'confirmPassword', 'email',
                    'phone', 'name', 'surname', 'parent_name', 'image', 'specialization_id'
                ],
                'required',
            ],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Паролі не співпадають'],
            ['email', 'email'],
            ['phone', 'string', 'max' => 14],
            ['email', 'checkEmailUnique'],
            ['username', 'checkUsernameUnique'],
            [['username', 'surname', 'name', 'parent_name'], 'string', 'max' => 32],
            ['password', 'string', 'min' => 4],
        ];
    }

    /**
     * @throws Exception
     */
    public  function __construct($user_id=null, $config=[])
    {
        if(!$user_id==null){
            $this->loadModel($user_id);
        }
        parent::__construct($config);
    }

    /**
     * @throws Exception
     */
    private function loadModel($user_id){
        $this->user_id=$user_id;
        $user=User::findOne($user_id);
        /* @var $doctor Doctor */
        $doctor=Doctor::find()->where(['user_id'=>$user_id])->one();
        if($user && $doctor){
            $this->username=$user->username;
            $this->email=$user->email;
            $this->phone=$user->phone;
            $this->specialization_id=$doctor->specialization_id;
            $this->image=$doctor->image;
            $this->name=$doctor->name;
            $this->surname=$doctor->surname;
            $this->parent_name=$doctor->parent_name;
            $this->isNewRecord=false;
        }
        else{
            throw new Exception('Лікара не знайдено');
        }
    }
    public function checkEmailUnique($attribute, $params)
    {
        $user = User::find()->where(['email' => $attribute])->one();
        if ($user) {
            $this->addError($attribute, Yii::t('app', 'Такий емайл вже існує'));
        }
    }

    public function checkUsernameUnique($attribute, $params)
    {
        $user = User::find()->where(['username' => $attribute])->one();
        if ($user) {
            $this->addError($attribute, Yii::t('app', 'Такий нікнейм вже існує'));
        }
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'username' =>Yii::t('app','Нікнейм'),
            'email' =>Yii::t('app','Емейл'),
            'phone' =>Yii::t('app','Телефон'),
            'name' =>Yii::t('app','Ім`я'),
            'surname' =>Yii::t('app','Прізвище'),
            'parent_name' =>Yii::t('app','По-батькові'),
            'image' =>Yii::t('app','Фото'),
            'specialization_id' =>Yii::t('app','Спеціалізація'),
        ];
    }

    /** @noinspection DuplicatedCode */
    /**
     * @throws \yii\db\Exception
     */
    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();
        if ($this->isNewRecord){
            $user = new User();
        }
        else{
            $user=User::findOne($this->user_id);
        }

        //записати в нього дані отримані
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->username = $this->username;
        $user->phone = $this->phone;
        $user->generateAuthKey();
        $user->role = RoleTypes::DOCTOR;
        $user->status = StatusTypes::ACTIVE;
        $user->save();
        //отримати user_id
        $user_id = $user->id;
        //Створити нову модель Doctor
        if($this->isNewRecord){
            $doctor = new Doctor();
        }
        else{
            $doctor=Doctor::find()->where(['user_id'=>$this->user_id])->one();
        }
        //записати дані в неї
        $doctor->name = $this->name;
        $doctor->surname = $this->surname;
        $doctor->parent_name = $this->parent_name;
        $doctor->specialization_id = $this->specialization_id;
        $doctor->user_id = $user_id;
        //завантажити аватарку
        $doctor->image =$this->downloadPhoto();
        //зберегти модель
        $doctor->save();
        //перевірити помилки
        if ($user->errors || $doctor->errors){
            $transaction->rollBack();
            var_dump($user->errors);
            var_dump($doctor->errors);
            die();
        }
        $transaction->commit();
        //якщо вони є то відмінити транзакцію
        //якщо немає закінчити транзакцію
        return true;
    }

    public function downloadPhoto(): ?string
    {
        $image = UploadedFile::getInstance($this, 'imageFile');
        if ($image) {
            $filename='uploads/doctors'.mktime().$image->getExtension();
            $image->saveAs($filename);
            return '/'.$filename;
        }
        if ($this->image){
            return $this->image;
        }
        return null;
    }
}
