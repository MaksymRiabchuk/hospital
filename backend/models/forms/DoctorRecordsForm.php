<?php

namespace backend\models\forms;

use common\models\entity\Doctor;
use common\models\entity\PreRecordsDoctors;
use DateTime;
use Exception;
use Yii;
use yii\base\Model;

class DoctorRecordsForm extends Model
{
    public $doctor;
    public $selectedDate;
    const MINUTES_INTERVAL=900;
    const EMPTY_COLOR='#99EEA1';
    const LOCK_COLOR='#EEAB99';
    private $array=[];
    private $startTime;
    private $endTime;

    /**
     * @throws Exception
     */
    public function __construct($config=null)
    {
        $doctor=Doctor::find()->one();
        /* @var $doctor Doctor */
        if(!$doctor){
            throw new Exception('Немає лікарів');
        }
        $this->doctor=$doctor->id;
        $this->selectedDate=strtotime(date('d.m.Y 00:00',time()));
        $this->findDoctorRecordsByDate();
        parent::__construct($config);
    }

    public function setDoctorAndDate($selectedDate,$selectedDoctor){
        if($selectedDate && $selectedDoctor){
            $this->selectedDate=$selectedDate;
            $this->doctor=$selectedDoctor;
        }
        $this->findDoctorRecordsByDate();
    }


    /**
     * @throws Exception
     */
    public function findDoctorRecordsByDate(){
        $this->findStartTime($this->selectedDate);
        $this->findEndTime($this->selectedDate);
        $records=PreRecordsDoctors::find()->where(['doctor_id'=>$this->doctor])->
        andWhere(['between','visit_date',$this->startTime,$this->endTime])->all();
        $this->array=[];
        for($i=$this->startTime;$i<$this->endTime;$i+=self::MINUTES_INTERVAL){
            $this->array[$i]=['color'=> Yii::$app->params['emptyColor']];
        }
        foreach ($records as $record){
            /* @var PreRecordsDoctors $record */
            $this->array[$record->visit_date]=['color'=> Yii::$app->params['lockColor']];
        }
    }

    /**
     * @throws Exception
     */
    private function findStartTime($start_day){
        $selectedDay=new DateTime(date('d.m.y',$start_day));
        $selectedDay=$selectedDay->format('Y-m-d 00:00');
        $this->startTime=strtotime($selectedDay);
    }

    /**
     * @throws Exception
     */
    private function findEndTime($start_day){
        $selectedDay=new DateTime(date('d.m.y',$start_day));
        $selectedDay=$selectedDay->format('Y-m-d 23:59');
        $this->endTime=strtotime($selectedDay);
    }
    public function viewTimeSchedule(): array
    {
        return $this->array;
    }

}