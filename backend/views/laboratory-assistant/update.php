<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\LaboratoryAssistant */

$this->title = 'Редагувати: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список лаборантів', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="laboratory-assistant-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
