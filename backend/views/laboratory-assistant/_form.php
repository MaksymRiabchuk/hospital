<?php /** @noinspection PhpUnhandledExceptionInspection */

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\entity\LaboratoryAssistant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="laboratory-assistant-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card card-default">
        <div class="card card-body">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'imageFile')->widget(FileInput::class,
                        [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => ($model->isNewRecord || !$model->image) ? false : $model->image,
                                'initialPreviewAsData' => true,
                                'showPreview' => true,
                                'showRemove' => false,
                                'showUpload' => false
                            ],
                        ]); ?>
                </div>
                <div class="col-md-8">
                    <div class="col-md-3">
                        <?= $form->field($model, 'username')->textInput(['maxlength' => true,'readonly'=>!$model->isNewRecord]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($model, 'parent_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <?php if ($model->isNewRecord): ?>
                    <div class="col-md-6">
                        <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'confirmPassword')->textInput(['maxlength' => true]) ?>
                    </div>
                    <?php endif;?>
                    <div class="col-md-6">
                        <?= $form->field($model, 'phone')->widget(MaskedInput::class,[
                            'name' => 'input-1',
                            'mask' => '(999) 999-9999'
                        ]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true,'readonly'=>!$model->isNewRecord]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>
