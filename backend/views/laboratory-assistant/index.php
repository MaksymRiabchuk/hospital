<?php /** @noinspection PhpUnhandledExceptionInspection */

/** @noinspection PhpUnusedParameterInspection */

use common\models\entity\LaboratoryAssistant;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\LaboratoryAssistantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список лаборантів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laboratory-assistant-index">

    <p>
        <?= Html::a('Добавити лаборанта', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'surname',
            'parent_name',
            [
                'attribute'=>'image',
                'filter'=>false,
                'format'=>'raw',
                'value'=>function($data){
                    /* @var $data LaboratoryAssistant */
                    return isset($data->image)? Html::img($data->image,['style'=>'max-width:75px']) :Yii::t('app','Фото немає');
                }
            ],
            [
                'class' => ActionColumn::class,
                'header'=> 'Дії',
                'template'=>'{update} {delete}',
                'urlCreator' => function($action, $model, $key, $index) {
                    return Url::to([$action,'id'=>$model->user_id]);
                },
            ],
        ],
    ]); ?>


</div>
