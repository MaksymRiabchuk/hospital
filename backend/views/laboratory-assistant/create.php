<?php

/* @var $this yii\web\View */
/* @var $model common\models\entity\LaboratoryAssistant */

$this->title = 'Добавити лаборанта';
$this->params['breadcrumbs'][] = ['label' => 'Список лаборантів ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laboratory-assistant-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
