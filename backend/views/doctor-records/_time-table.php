<?php

use common\models\classes\FormattingRecord;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $schedule array */
/* @var $day int */
/* @var $doctor_id int */
$this->title = 'Записи лікаря';
$this->params['breadcrumbs'][] = $this->title;
//проблема з кольорами
?>
<table class="table table-bordered">
    <tbody>
    <tr>
        <?php $k = 0; ?>
        <?php foreach ($schedule as $key => $time): ?>
        <?php $k++ ?>
        <?php isset($time['color']) ? $color = $time['color'] : $color = FormattingRecord::EMPTY_COLOR ?>
        <?php if ($color == FormattingRecord::LOCK_COLOR): ?>
            <?= '<td style="background-color:' . $color . ' ">' ?>
            <?= Html::a(date('H:i', $key),'#',['class'=>'get-info-url','data-datetimestamp'=>$key]) ; ?>
        <?php else: ?>
            <?= '<td style="background-color:' . $color . ' ">' ?>
            <span data-dateTimeStamp="<?= $key ?>" class="record-time">
                                <?= date('H:i', $key); ?>
                            </span>
        <?php endif; ?>
        <?php if ($key): ?>

        <?php else: echo 'Дня не іcнує' ?>
        <?php endif; ?>
        <?php if ($k % 12 == 0): ?>
    </tr>
    <?php endif ?>
    <?php endforeach; ?>

    </tbody>
</table>

