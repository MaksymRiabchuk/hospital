<?php

use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\entity\PreRecordsDoctors;
use common\models\enums\PreRecordsStatusTypes;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $record PreRecordsDoctors */
/* @var $patient Patient */
$this->title=Yii::t('app','Інформацію про запис')
?>

<div class="schedule-form">
    <p>
        <?= Yii::t('app', 'Лікар:') ?>

        <?= Doctor::getDoctorNameById($record->doctor_id) ?>
    </p>
    <p>
        <?= Yii::t('app', 'Вибраний час:') ?>
        <?= date('Y.m.d H:i', $record->visit_date) ?>
    </p>
    <p>
        <?= Yii::t('app', 'Пацієнт:') ?>
        <?= Yii::t('app', $patient->getFullPatientName()) ?>
    </p>
    <p>
        <?= PreRecordsStatusTypes::getLabel($record->status); ?>
    </p>
</div>