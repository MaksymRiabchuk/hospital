<?php /** @noinspection DuplicatedCode */


/* @var $this yii\web\View */
/* @var $day int */

/* @var $model FormattingRecord */

use common\models\classes\FormattingRecord;
use common\models\entity\Doctor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Графіки лікарів';
$this->params['breadcrumbs'][] = $this->title;
$model->selectedDate = date('Y-m-d', $model->selectedDate)
?>
<div class="hospital-schedule-index">
    <div class="card card-default">
        <div class="card card-header">
            <?php $form = ActiveForm::begin() ?>
            <div class="row">
                <div class="col-md-5">
                    <?= $form->field($model, 'doctor')->dropDownList(Doctor::getDoctorsList()) ?>
                </div>
                <div class="col-md-5">
                    <?= $form->field($model, 'selectedDate')->textInput(['type' => 'date']) ?>
                </div>
                <div class="col-md-2" style="margin-top: 25px">
                    <a class="btn btn-primary" id="find-doctor-records-btn">
                        Найти
                    </a>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="card card-body">
            <div id="select-time-table">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <?php $k = 0; ?>
                        <?php foreach ($model->viewTimeSchedule() as $key => $time): ?>
                        <?php $k++ ?>
                        <?php isset($time['color']) ? $color = $time['color'] : $color = FormattingRecord::EMPTY_COLOR ?>
                        <?php if ($color == FormattingRecord::LOCK_COLOR): ?>
                            <?= '<td style="background-color:' . $color . ' ">' ?>
                            <?= Html::a(date('H:i', $key),'#',['class'=>'get-info-url','data-datetimestamp'=>$key]) ; ?>
                        <?php else: ?>
                            <?= '<td style="background-color:' . $color . ' ">' ?>
                            <span data-dateTimeStamp="<?= $key ?>" class="record-time">
                                <?= date('H:i', $key); ?>
                            </span>
                        <?php endif; ?>
                        <?php if ($key): ?>

                        <?php else: echo 'Дня не іcнує' ?>
                        <?php endif; ?>
                        <?php if ($k % 12 == 0): ?>
                    </tr>
                    <?php endif ?>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>