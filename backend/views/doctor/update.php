<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\Doctor */

$this->title = Yii::t('app','Редагування лікаря: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Список лікарів'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app','Редагування');
?>
<div class="doctor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
