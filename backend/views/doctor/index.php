<?php /** @noinspection PhpUnhandledExceptionInspection */

/** @noinspection PhpUnusedParameterInspection */

use common\models\entity\Doctor;
use common\models\entity\Specialization;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DoctorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Список лікарів');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doctor-index">

    <p>
        <?= Html::a(Yii::t('app','Добавити лікаря'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'surname',
            'parent_name',
            [
                    'attribute'=>'specialization_id',
                    'filter'=> Specialization::getSpecializationsById(),
                    'format'=>'raw',
                    'value'=>function($data){
                        /* @var $data Doctor */
                        return $data->specialization->name ?? Yii::t('app', 'Немає спеціалізації');
                    }
            ],
            [
                    'attribute'=>'image',
                    'filter'=>false,
                    'format'=>'raw',
                    'value'=>function($data){
                        /* @var $data Doctor */
                        return isset($data->image)? Html::img($data->image,['style'=>'max-width:75px']) :Yii::t('app','Фото немає');
                    }
            ],
            'user.phone',
            [
                'class' => ActionColumn::class,
                'header'=> 'Дії',
                'template'=>'{update} {delete}',
                'urlCreator' => function($action, $model, $key, $index) {
                    return Url::to([$action,'id'=>$model->user_id]);
                },
            ],
        ],
    ]); ?>


</div>
