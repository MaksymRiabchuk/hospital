<?php


/* @var $this yii\web\View */
/* @var $model backend\models\EditDoctor */

$this->title = Yii::t('app','Добавити лікаря');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Список лікарів'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doctor-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
