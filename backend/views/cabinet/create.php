<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\Cabinet */

$this->title = Yii::t('app','Збергети кабінет');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Список кабінетів'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabinet-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
