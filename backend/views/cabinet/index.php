<?php

use common\models\entity\Cabinet;
use yii\helpers\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CabinetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Список кабінетів');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabinet-index">

    <p>
        <?= Html::a(Yii::t('app','Добавити кабінет'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'description:text',
            [
                'attribute'=>'is_deleted',
                'filter'=>[1=>Yii::t('app','Так'),0=>Yii::t('app','Ні')],
                'format'=>'raw',
                'value'=>function($data){
                    /* @var $data Cabinet */
                    if($data->is_deleted==0){
                        return '<span class="fa fa-ban" style="color: indianred"></span>';
                    }
                    else{
                        return '<span class="fa fa-check" style="color: lightgreen"></span>';
                    }
                },
            ],
            [
                'class' => ActionColumn::class,
                'header'=>Yii::t('app','Дії'),
                    'template'=>'{update} {delete}'
            ],
        ],
    ]); ?>


</div>
