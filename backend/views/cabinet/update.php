<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\Cabinet */

$this->title = Yii::t('app','Редагування кабінету: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' =>Yii::t('app','Список кабінетів'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app','Редагувати');
?>
<div class="cabinet-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]);?>

</div>
