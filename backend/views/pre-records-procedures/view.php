<?php /** @noinspection PhpUnhandledExceptionInspection */

use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\entity\PreRecordsProcedures;
use common\models\entity\ProcedureTypes;
use common\models\enums\ProcedureVisitStatus;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\entity\PreRecordsProcedures */

$this->title = $model->procedureType->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Список процедур'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="doctor-view">


    <p>
        <?= Html::a(Yii::t('app', 'Редагувати'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Видалити'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Ви дійсно хочете видалити елемент?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=>'patient_id',
                'filter'=>Patient::getPatientsList(),
                'value'=>function($data){
                    /* @var $data PreRecordsProcedures*/
                    return isset($data->patient_id)?
                        $data->patient->surname.' '.$data->patient->name.' '.$data->patient->parent_name:
                        'Такого пацієнта не існує';
                }
            ],
            [
                'attribute'=>'doctor_id',
                'filter'=> Doctor::getDoctorsList(),
                'value'=>function($data){
                    /* @var $data PreRecordsProcedures*/
                    return isset($data->doctor_id)?
                        $data->doctor->surname.' '.$data->doctor->name.' '.$data->doctor->parent_name:
                        'Такого лікаря не існує';
                }
            ],
            [
                'attribute'=>'visit_date',
                'filter'=>false,
                'format' => ['datetime', 'php:d.m.Y H:i'],
            ],
            [
                'attribute'=>'procedure_id',
                'filter'=> ProcedureTypes::getProcedureTypesById(),
                'value'=>function($data){
                    /* @var $data PreRecordsProcedures */
                    return isset($data->procedureType)? $data->procedureType->name:'Такої процедури не існує';
                }
            ],
            [
                    'attribute'=>'status',
                    'filter'=> ProcedureVisitStatus::listData(),
                    'value'=>function($data){
                        /* @var $data PreRecordsProcedures */
                        return ProcedureVisitStatus::getLabel($data->status);
                    }
            ],
        ],
    ]) ?>

</div>
