<?php /** @noinspection PhpUnusedParameterInspection */

/** @noinspection PhpUnhandledExceptionInspection */

use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\entity\PreRecordsProcedures;
use common\models\entity\ProcedureTypes;
use common\models\enums\ProcedureVisitStatus;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PreRecordsProceduresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Записи на процедури';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="patient-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                    'attribute'=>'patient_id',
                    'filter'=>Patient::getPatientsList(),
                    'value'=>function($data){
                        /* @var $data PreRecordsProcedures*/
                        return isset($data->patient_id)?
                            $data->patient->surname.' '.$data->patient->name.' '.$data->patient->parent_name:
                            'Такого пацієнта не існує';
                    }
            ],
            [
                'attribute'=>'doctor_id',
                'filter'=> Doctor::getDoctorsList(),
                'value'=>function($data){
                    /* @var $data PreRecordsProcedures*/
                    return isset($data->doctor_id)?
                        $data->doctor->surname.' '.$data->doctor->name.' '.$data->doctor->parent_name:
                        'Такого лікаря не існує';
                }
            ],
            [
                'attribute'=>'visit_date',
                'filter'=>false,
                'format' => ['datetime', 'php:d.m.Y H:i'],
            ],
            [
                'attribute'=>'procedure_type_id',
                'filter'=> ProcedureTypes::getProcedureTypesById(),
                'value'=>function($data){
                    /* @var $data PreRecordsProcedures */
                    return isset($data->procedureType)? $data->procedureType->name:'Такої процедури не існує';
                }
            ],
            [
                    'attribute'=>'status',
                    'filter'=> ProcedureVisitStatus::listData(),
                    'value'=>function($data){
                        /* @var $data PreRecordsProcedures */
                        return ProcedureVisitStatus::getLabel($data->status);
                    }
            ],
            [
                'class' => ActionColumn::class,
                'template'=>'{view}',
                'urlCreator' => function ($action, PreRecordsProcedures $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
