<?php /** @noinspection PhpUnhandledExceptionInspection */

use yii\helpers\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ProcedureTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типи процедур';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procedures-name-index">

    <p>
        <?= Html::a('Добавити тип процедури', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'class' => ActionColumn::class,
                'template'=>'{update} {delete}'
            ],
        ],
    ]); ?>


</div>
