<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\ProcedureTypes */

$this->title = 'Добавити тип процедури';
$this->params['breadcrumbs'][] = ['label' => 'Типи процедур', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procedures-name-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
