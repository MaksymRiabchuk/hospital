<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\ProcedureTypes */

$this->title = 'Update Procedures Name: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типи процедур', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="procedures-name-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
