<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\Specialization */

$this->title = Yii::t('app','Добавити спеціалізацію') ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Список спеціалізацій'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialization-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
