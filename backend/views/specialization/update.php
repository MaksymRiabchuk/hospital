<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\Specialization */

$this->title = Yii::t('app','Редагування спеціалізації: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Список спеціалізацій'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app','Редагувати');
?>
<div class="specialization-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
