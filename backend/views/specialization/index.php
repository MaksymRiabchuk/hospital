<?php /** @noinspection PhpUnhandledExceptionInspection */

use common\models\entity\Department;
use common\models\entity\Specialization;
use yii\helpers\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SpecializationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Список спеціалізацій');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialization-index">

    <p>
        <?= Html::a(Yii::t('app', 'Добавити спеціалізацію'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'department_id',
                'filter'=> Department::getDepartmentsById(),
                'format'=>'raw',
                'value'=>function($data){
                    /* @var $data Specialization */
                    return $data->department->name ?? 'Відділення немає';
                }
            ],
            [
                'attribute'=>'is_deleted',
                'filter'=>[1=>'Так',0=>'Ні'],
                'format'=>'raw',
                'value'=>function($data){
                    /* @var $data Specialization */
                    if($data->is_deleted==0){
                        return '<span class="fa fa-ban" style="color: indianred"></span>';
                    }
                    else{
                        return '<span class="fa fa-check" style="color: lightgreen"></span>';
                    }
                },
            ],
            [
                'class' => ActionColumn::class,
                'header' => 'Дії',
            ],
        ],
    ]); ?>


</div>
