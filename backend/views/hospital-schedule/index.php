<?php

use common\models\classes\FormattingSchedule;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Графік лікарні';
$this->params['breadcrumbs'][] = $this->title;
$schedule = new FormattingSchedule();
?>

<div class="hospital-schedule-index">

    <table class="table table-bordered">
        <thead>
        <tr>
            <td>
                Понеділок
            </td>
            <td>
                Вівторок
            </td>
            <td>
                Середа
            </td>
            <td>
                Четвер
            </td>
            <td>
                П'ятниця
            </td>
            <td>
                Субота
            </td>
            <td>
                Неділя
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <?php $k = 0; ?>
            <?php foreach ($schedule->viewSchedule() as $key => $day): ?>
            <?php $k++ ?>
<!--            --><?php //var_dump($schedule->viewSchedule()); ?>
<!--            --><?php //die()?>
            <?php if ($day): ?>
                <?php isset($day['url'])? $url=$day['url']:$url='#';?>
                <?php isset($day['color']) ? $color = $day['color'] : $color = '#EEAB99' ?>
                <?= '<td style="background-color:' . $color . ' ">' ?>
                <?php if ($day['dayTimeStamp'] === false): ?>
                    <?= date('d.m.Y',$key) ?>
                <?php else: ?>
                    <span style="font-weight: bold">
                        <?= date('d.m.Y',$key)?>
                    </span>
                <?php endif; ?>
            <?php else: echo 'Дня не інє' ?>
            <?php endif; ?>
            <?php if ($k % 7 == 0): ?>
        </tr>
        <?php endif ?>
        <?php endforeach; ?>
        </tbody>
    </table>


</div>
