<?php

use common\models\enums\DepartmentFloors;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Department */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="departments-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card card-body">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'address')->dropDownList([
                        DepartmentFloors::listData(),
                    ]) ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app','Зберегти'), ['class' => 'btn btn-success']) ?>
            </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
