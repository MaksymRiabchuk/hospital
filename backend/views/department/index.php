<?php /** @noinspection PhpUnhandledExceptionInspection */

use common\models\entity\Department;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список відділень';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departments-index">

    <p>
        <?= Html::a('Добавити відділення', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'address',
            [
                    'attribute'=>'is_deleted',
                    'filter'=>[1=>'Так',0=>'Ні'],
                    'format'=>'raw',
                    'value'=>function($data){
                        /* @var $data Department */
                        if($data->is_deleted==0){
                            return '<span class="fa fa-ban" style="color: indianred"></span>';
                        }
                        else{
                            return '<span class="fa fa-check" style="color: lightgreen"></span>';
                        }
                    },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Дії',
                'template'=>'{update} {delete}',
            ],
        ],
    ]); ?>


</div>
