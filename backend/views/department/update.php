<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\Department */

$this->title = 'Редагування відділення: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список відділень', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="departments-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
