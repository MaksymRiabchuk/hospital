<?php


/* @var $this yii\web\View */
/* @var $model common\models\entity\Department */

$this->title = Yii::t('app','Добавити відділення');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Список відділень') , 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departments-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
