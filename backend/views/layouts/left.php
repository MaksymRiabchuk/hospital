<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Статистика', 'icon' => 'file-code-o', 'url' => ['/dashboard']],
                    ['label' => 'Відділення', 'icon' => 'file-code-o', 'url' => ['/department/index']],
                    ['label' => 'Користувачі', 'icon' => 'file-code-o', 'url' => ['/user/index']],
                    ['label' => 'Спеціалізації', 'icon' => 'file-code-o', 'url' => ['/specialization/index']],
                    ['label' => 'Кабінети', 'icon' => 'file-code-o', 'url' => ['/cabinet/index']],
                    ['label' => 'Лікарі', 'icon' => 'file-code-o', 'url' => ['/doctor/index']],
                    ['label' => 'Лаборант', 'icon' => 'file-code-o', 'url' => ['/laboratory-assistant/index']],
                    ['label' => 'Пацієнти', 'icon' => 'file-code-o', 'url' => ['/patients/index']],
                    ['label' => 'Записи на процедури', 'icon' => 'file-code-o', 'url' => ['/pre-records-procedures/index']],
                    ['label' => 'Типи процедур', 'icon' => 'file-code-o', 'url' => ['/procedure-types/index']],
                    ['label' => 'Графік роботи лікарні', 'icon' => 'file-code-o', 'url' => ['/hospital-schedule/index']],
                    ['label' => 'Графік роботи', 'icon' => 'file-code-o', 'url' => ['/doctor-records/index']],
                ],
            ]
        ) ?>

    </section>

</aside>
