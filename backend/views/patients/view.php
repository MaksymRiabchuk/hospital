<?php

use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\entity\Procedure;
use common\models\entity\ProceduresName;
use common\models\entity\Specialization;
use common\models\enums\ProcedureVisitStatus;
use common\models\User;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\entity\Patient */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Список пацієнтів '), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="doctor-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'parent_name',
            [
                'attribute'=>'image',
                'filter'=>false,
                'format'=>'raw',
                'value'=>function($data){
                    /* @var $data Patient */
                    return $data->image ? Html::img($data->image,['style'=>'max-width:75px']) :Yii::t('app','Фото немає');
                }
            ],
            [
                'attribute' => 'user_id',
                'filter'=> User::getUsersById(),
                'value'=>function($data){
                    /* @var $data Patient */
                    return isset($data->user_id)? $data->user->username :'Такого користувача не існує';
                }
            ],
        ],
    ]) ?>

</div>
