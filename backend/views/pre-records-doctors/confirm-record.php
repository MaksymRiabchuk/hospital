<?php

use common\models\entity\Doctor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\entity\PreRecordsDoctors */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-form">
    <p>
        <?= Yii::t('app', 'Ваш лікар:') ?>

        <?= Doctor::getDoctorNameById($model->doctor_id) ?>
    </p>
    <p>
        <?= Yii::t('app', 'Вибраний час:') ?>
        <?= date('Y.m.d H:i',$model->visit_date) ?>
    </p>
    <?php $form = ActiveForm::begin() ?>
        <?= $form->field($model,'comment')->textarea(['rows'=>5]) ?>
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>
    <?php ActiveForm::end() ?>
</div>