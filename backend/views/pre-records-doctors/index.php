<?php

use common\models\classes\FormattingRecord;
use common\models\entity\Doctor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model FormattingRecord */
/* @var $dayTimeStamp int */
/* @var $doctor_id int */
/* @var $time int */
$this->title = 'Hospital Schedules';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="hospital-schedule-index">
        <div class="row">
            <div class="col-md-5 choose-doctor">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'doctor')->dropDownList(Doctor::getDoctorsList()) ?>
                <?php ActiveForm::end() ?>
            </div>
            <div id="select-time-table">
                <table class="table table-bordered">

                    <tbody>
                    <tr>
                        <?php $k = 0; ?>
                        <?php foreach ($model->viewTimeSchedule() as $key => $time): ?>
                        <?php $k++ ?>
                        <?php isset($time['color']) ? $color = $time['color'] : $color = FormattingRecord::EMPTY_COLOR ?>
                        <?= '<td style="background-color:' . $color . ' ">' ?>
                        <?= Html::a(date('H:i', $key), [
                            '/schedule-records-doctors/confirm-record',
                            'dayTimeStamp' => $dayTimeStamp,
                            'doctor_id'=>$doctor_id,
                            'time'=>$key,
                        ],
                            ['style' => 'background-color:' . $color . ';border: solid' . $color . '1px;width:45px;']) ?>
                        <?php if ($key): ?>

                        <?php else: echo 'Дня не іcнує' ?>
                        <?php endif; ?>
                        <?php if ($k % 12 == 0): ?>
                    </tr>
                    <?php endif ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


<?php
$script = <<<JS
let selectedDoctor;
let selectedTime;
$("#formattingrecord-doctor").on('change',function (){
    selectedDoctor=$(this).val()
    $.ajax({
        url:"/schedule-records-doctors/change-doctor",
        type:'post',
        dataType:'JSON',
        data:{doctor_id:selectedDoctor,day:'$key'},
        success:function (data){
            $('#select-time-table').html(data);
        }
    })
})
$(".record-time").on('click',function (){
    if(selectedDoctor==null){
        alert('Виберіть лікаря');
    }
    else{
        let color=$(this).css('background-color');
        if(color=='rgb(238, 171, 153)'){
            alert('Цей час вже зайнятий')
        }
        else{
            selectedTime=$(this).attr('data-record-time');
            $.ajax({
                url: "confirm-record",
                type: 'post',
                dataType: 'JSON',
                data:{doctor_id:selectedDoctor,created_at:selectedTime},
                success: function(response) {
                    $('#select-time-table').html(response)
                }
            });
        }
    }
})
JS;
$this->registerJs($script);
