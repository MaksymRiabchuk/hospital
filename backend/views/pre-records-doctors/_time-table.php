<?php

use common\models\classes\FormattingRecord;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $schedule array */
/* @var $day int */
/* @var $doctor_id int */
$this->title = 'Hospital Schedules';
$this->params['breadcrumbs'][] = $this->title;
?>

<table class="table table-bordered">
    <tbody>
    <tr>
        <?php $k = 0; ?>
        <?php foreach ($schedule

        as $key => $time): ?>
        <?php $k++ ?>
        <?php isset($time['color']) ? $color = $time['color'] : $color = FormattingRecord::EMPTY_COLOR ?>
        <?= '<td style="background-color:' . $color . ' ">' ?>
        <?= Html::a(date('H:i', $key), [
            '/schedule-records-doctors/confirm-record',
            'day' => $day,
            'doctor_id' => $doctor_id,
            'time'=>$key,
        ],
            ['style' => 'background-color:' . $color . ';border: solid' . $color . '1px;width:45px;']) ?>
        <?php if ($key): ?>

        <?php else: echo 'Дня не іcнує' ?>
        <?php endif; ?>
        <?php if ($k % 12 == 0): ?>
    </tr>
    <?php endif ?>
    <?php endforeach; ?>
    </tbody>
</table>

