<?php

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $user User */
/* @var $user_permit array */
/* @var $roles array */

$this->title = 'Update User: '.$user->username ;
$this->params['breadcrumbs'][] = ['label' => 'Користувачі', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Права';
?>
<div class="user-update">


    <?php $form = ActiveForm::begin(['action' => ["grants", 'id' => $user->getId()]]); ?>
    <?= Html::hiddenInput('withoutRights','withoutRights');?>
    <?= Html::checkboxList('roles', $user_permit, $roles, ['separator' => '<br>']); ?>

    <div class="form-group">
        <?= Html::submitButton('Зберегти', ['class' => 'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>