<?php

use common\models\enums\RoleTypes;
use common\models\enums\StatusTypes;
use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список користувачів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <p>
        <?= Html::a('Добавити користувача', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= /** @noinspection PhpUnhandledExceptionInspection */
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                    'attribute'=>'username',
                    'format'=>'raw',
                    'value'=>function($data){
                        /* @var $data User */
                        return Html::a($data->username,['user/grants','id'=>$data->id],['style'=>'color:black']);
                    }
            ],
            [
                    'label'=>'ПІБ',
                    'value'=>function($data){
                        /* @var $data User */
                        if($data->role==RoleTypes::DOCTOR){
                            return isset($data->doctor)?$data->doctor->shortDoctorName:
                                Yii::t('app','Такого профілю не існує');
                        }
                        if($data->role==RoleTypes::LABORATORY_ASSISTANT){
                            return isset($data->laboratoryAssistant)?$data->laboratoryAssistant->shortLaboratoryAssistantName:
                                Yii::t('app','Такого профілю не існує');
                        }
                        if($data->role==RoleTypes::PATIENT){
                            return  isset($data->patient)?$data->patient->shortPatientName:
                                Yii::t('app','Такого профілю не існує ');
                        }
                        if($data->role==RoleTypes::ADMIN){
                            return $data->username;
                        }
                        return '';
                    },
            ],
            'email:email',
            'phone',
            [
                'attribute'=>'status',
                'filter'=> StatusTypes::listData(),
                'value'=>function($data){
                    /* @var $data User */
                    return StatusTypes::getLabel($data->status);
                },
            ],
            [
                'attribute'=>'role',
                'filter'=> RoleTypes::listData(),
                'value'=>function($data){
                    /* @var $data User */
                    return RoleTypes::getLabel($data->role);
                },
            ],
        ],
    ]); ?>


</div>
