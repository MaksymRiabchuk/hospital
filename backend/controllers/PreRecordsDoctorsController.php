<?php /** @noinspection PhpUnused */

namespace backend\controllers;

use common\models\classes\FormattingRecord;
use common\models\entity\Doctor;
use common\models\entity\PreRecordsDoctors;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class PreRecordsDoctorsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all HospitalSchedule models.
     *
     * @param $user_id
     * @param $dayTimeStamp
     * @return string
     */
    public function actionIndex($user_id, $dayTimeStamp): string
    {
        $doctor = Doctor::find()->one();
        /*  @var $doctor Doctor */
        $model = new FormattingRecord($dayTimeStamp, $doctor->id);
        $array = Doctor::getAllDoctorId();
        return $this->render('index', [
            'model' => $model,
            'user_id' => $user_id,
            'doctor_id' => $doctor->id,
            'array' => $array,
            'dayTimeStamp' => $dayTimeStamp,
        ]);
    }

    /** @noinspection DuplicatedCode */
    public function actionChangeDoctor(): string
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $day = Yii::$app->request->post('day');
        $doctor_id = Yii::$app->request->post('doctor_id');
        $schedule = new FormattingRecord($day, $doctor_id);
        return $this->renderPartial('_time-table', [
            'day' => Yii::$app->request->post('day'),
            'doctor_id' => Yii::$app->request->post('doctor_id'),
            'schedule' => $schedule->viewTimeSchedule(),
        ]);
    }

    public function actionConfirmRecord($doctor_id,$time)
    {
        $model=new PreRecordsDoctors();
        $model->visit_date=$time;
        $model->doctor_id=$doctor_id;
        $model->user_id=Yii::$app->user->getId();
        if ($model->load($this->request->post())) {
            if($model->save()){
                return $this->redirect(['index']);
            }
            else{
                return $this->render('error',['model'=>$model]);
            }

        }

        return $this->render('confirm-record', [
            'model'=>$model
        ]);
    }
}
