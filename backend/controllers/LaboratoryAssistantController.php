<?php /** @noinspection PhpUnused */

namespace backend\controllers;

use backend\models\EditLaboratoryAssistant;
use common\models\entity\LaboratoryAssistant;
use common\models\search\LaboratoryAssistantSearch;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * LaboratoryAssistantController implements the CRUD actions for LaboratoryAssistant model.
 */
class LaboratoryAssistantController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all LaboratoryAssistant models.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new LaboratoryAssistantSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new LaboratoryAssistant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     * @throws Exception
     */
    public function actionCreate()
    {
        $model = new EditLaboratoryAssistant();

        if ($model->load($this->request->post())) {
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LaboratoryAssistant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|Response
     * @throws Exception
     * @throws \Exception
     */
    public function actionUpdate(int $id)
    {
        $model = new EditLaboratoryAssistant($id);

        if ($this->request->isPost && $model->load($this->request->post())) {
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LaboratoryAssistant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return Response
     * @throws NotFoundHttpException|StaleObjectException if the model cannot be found
     */
    public function actionDelete(int $id): Response
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LaboratoryAssistant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return LaboratoryAssistant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): LaboratoryAssistant
    {
        if (($model = LaboratoryAssistant::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
