<?php /** @noinspection PhpUnused */

namespace backend\controllers;

use backend\models\forms\DoctorRecordsForm;
use common\models\classes\FormattingRecord;
use common\models\entity\Doctor;
use common\models\entity\Patient;
use common\models\entity\PreRecordsDoctors;
use modules\patient\Module;
use Yii;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class DoctorRecordsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }
    /** @noinspection DuplicatedCode */
    /**
     * Lists all HospitalSchedule models.
     *
     * @return string
     * @throws \Exception
     */
    public function actionIndex(): string
    {
        $model=new DoctorRecordsForm();
//        $doctor = Doctor::find()->one();
//        if (!$doctor){
//            throw new Exception('Такого лікаря не інсує');
//        }
//        /* @var $doctor Doctor */
//        $model = new FormattingRecord(time(), $doctor->id);
//        $array = Doctor::getAllDoctorId();
        return $this->render('index', [
            'model' => $model,
            'array'=>$model->viewTimeSchedule(),
        ]);
    }
    public function actionChangeDoctor()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $day = Yii::$app->request->post('day',date('Y-m-d'));
        if(!Doctor::find()->one()){
            throw new Exception('Лікарів не існує');
        }
        $doctor_id = Yii::$app->request->post('doctor_id',Doctor::find()->one());
        if($day and $doctor_id){
            $schedule = new DoctorRecordsForm();
            $schedule->setDoctorAndDate(strtotime($day),$doctor_id);
            return $this->renderPartial('_time-table', [
                'day' =>$day,
                'doctor_id' =>$doctor_id,
                'schedule' => $schedule->viewTimeSchedule(),
            ]);
        }
        else {
            return false;
        }
    }



    /**
     * @throws \Exception
     */

    public function actionGetInfo($doctor_id,$time)
    {
        $record=PreRecordsDoctors::find()->where(['visit_date'=>$time])->andWhere(['doctor_id'=>$doctor_id])->one();
        if(!$record){
            throw new \Exception(Yii::t('app','Такого запису не снує'));
        }
        /* @var $record PreRecordsDoctors */
        $patient=Patient::find()->where(['user_id'=>$record->user_id])->one();
        return $this->render('get-info', [
            'record'=>$record,
            'patient'=>$patient,
        ]);
    }

}
