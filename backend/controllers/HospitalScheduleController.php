<?php /** @noinspection PhpUnused */

namespace backend\controllers;

use common\models\classes\FormattingSchedule;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * HospitalScheduleController implements the CRUD actions for HospitalSchedule model.
 */
class HospitalScheduleController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all HospitalSchedule models.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $data=new FormattingSchedule();
        return $this->render('index', [
            'data' => $data,
        ]);
    }
}
