let selectedDoctor=$("#doctorrecordsform-doctor").val();
let selectedTime=$("#doctorrecordsform-selecteddate").val();
$("#find-doctor-records-btn").on('click',function(){
    if(selectedDoctor==null){
        alert('Виберіть лікаря')
    }
    else if(selectedTime==null){
        alert('Виберіть дату')
    }
    else{
        $.ajax({
            url:"/doctor-records/change-doctor",
            type:'POST',
            dataType:'JSON',
            data:{doctor_id:selectedDoctor,day:selectedTime},
            success:function (data){
                $('#select-time-table').html(data);
            },
            errors:function (data){
                console.log(data);
            }
        })
    }

})
$("#doctorrecordsform-doctor").on('change',function (){
    selectedDoctor=$(this).val();
})
$("#doctorrecordsform-selecteddate").on('change',function (){
    selectedTime=$(this).val();
})
$(document).on('click',".get-info-url",function (){
    let selectedTime2=$(this).attr('data-datetimestamp');
    window.location.href="http://admin-hospital/doctor-records/get-info?doctor_id="+selectedDoctor+"&time="+selectedTime2;
})
//$(".record-time").on('click',function (){

//    else{
//        let color=$(this).css('background-color');
//        if(color=='rgb(238, 171, 153)'){
//            alert('Цей час вже зайнятий')
//        }
//        else{
//            selectedTime=$(this).attr('data-record-time');
//            $.ajax({
//                url: "confirm-record",
//                type: 'post',
//                dataType: 'JSON',
//                data:{doctor_id:selectedDoctor,created_at:selectedTime},
//                success: function(response) {
//                    $('#select-time-table').html(response)
//                }
//            });
//        }
//    }
//})